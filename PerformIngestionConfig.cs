﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016
{
    public static class PerformIngestionConfig
    {
       //static int _backdateday, _backdatedmonth, _backdatedyear;
       // static int _futureday, _futuremoth, _futureyear;
        static string _firstDateString, _secondDateString;
        static DateTime _previousDate,_futureDate;
        static bool _enableLineuUp;
        static bool _enableSquadDetail;
        static string _pagerDutyEndpoint;
        static bool _pagerDutyEnable;
        static string _pagerDutyKey;
        public static string FirstDateString
        {
            get { return _firstDateString; }
        }
        public static DateTime FirstDate
        {
            get { return _previousDate; }
        }

        public static string SecondDateString
        {
            get { return _secondDateString; }
        }
        public static DateTime SecondDate
        {
            get { return _futureDate; }
        }

        public static bool EnableMatchDetailLineUp
        {
            get { return _enableLineuUp; }
        }
        public static bool EnableSquadDetail
        {
            get { return _enableSquadDetail; }
        }

        public static bool PagerDutyEnable
        {
            get { return _pagerDutyEnable; }
        }
        public static string PagerDutyKey
        {
            get { return _pagerDutyKey; }
        }

        public static string PagerDutyEndpoint
        {
            get { return _pagerDutyEndpoint; }
        }
        public static void Initialize()
       {
            string enableDateRange = ConfigurationManager.AppSettings["enableDateRange"];
            string enableLineUp = ConfigurationManager.AppSettings["enableMatchDetailLineup"];
            string enableSquadDetail = ConfigurationManager.AppSettings["enableSquadDetail"];
            string pagerDutyEndpoint;
            string enablePagerDuty;
            
            {
                if (enableDateRange == "yes")
                {
                    int buffer;
                    _previousDate = DateTime.Now;
                    _futureDate = DateTime.Now;
                    string backdatedday = ConfigurationManager.AppSettings["backdatedday"];
                    //string backdatedmonth = ConfigurationManager.AppSettings["backdatedmonth"];
                    //string backdatedyear = ConfigurationManager.AppSettings["backdatedyear"];

                    string futureday = ConfigurationManager.AppSettings["futureday"];
                    //string futuremonth = ConfigurationManager.AppSettings["futuremonth"];
                    //string futureyear = ConfigurationManager.AppSettings["futureyear"];

                    if(int.TryParse(backdatedday,out buffer))
                    {
                        _previousDate = _previousDate.AddDays(buffer*-1);
                    }
                    //if (int.TryParse(backdatedmonth, out buffer))
                    //{
                    //    _previousDate = _previousDate.AddMonths(buffer*-1);
                    //}
                    //if (int.TryParse(backdatedyear, out buffer))
                    //{
                    //    _previousDate = _previousDate.AddYears(buffer*-1);
                    //}
                    
                    _firstDateString = _previousDate.ToString("yyyy-MM-ddT00:00:00Z");

                    if (int.TryParse(futureday, out buffer))
                    {
                        _futureDate = _futureDate.AddDays(buffer);
                    }
                    //if (int.TryParse(futuremonth, out buffer))
                    //{
                    //    _futureDate = _futureDate.AddMonths(buffer);
                    //}
                    //if (int.TryParse(futureyear, out buffer))
                    //{
                    //    _futureDate = _futureDate.AddYears(buffer);
                    //}
                    
                    _secondDateString = _futureDate.ToString("yyyy-MM-ddT11:59:59Z");
                }
            }

            if (!string.IsNullOrEmpty(enableLineUp))
            {
                _enableLineuUp = enableLineUp == "yes" ? true:false;
            }

            if (!string.IsNullOrEmpty(enableSquadDetail))
            {
                _enableSquadDetail = enableSquadDetail == "yes" ? true : false;
            }

            var competitionList = ConfigurationManager.GetSection("pagerduty") as NameValueCollection;
            if (competitionList != null)
            {
                pagerDutyEndpoint = competitionList["endpoint"];
                enablePagerDuty = competitionList["enable"];
                _pagerDutyKey = competitionList["integrationkey"];
                if (!string.IsNullOrEmpty(pagerDutyEndpoint))
                {
                    _pagerDutyEndpoint = pagerDutyEndpoint;
                    if (!string.IsNullOrEmpty(enablePagerDuty))
                    {
                        _pagerDutyEnable = enablePagerDuty == "yes" ? true : false;
                    }
                    else
                        _pagerDutyEnable = false;

                }
                else
                {
                    _pagerDutyEnable = false;
                }
            }

          
                
        }
    }
}
