﻿using PerformSDAPIDataIngest2016.PagerDuty;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using PerformSDAPIDataIngest2016.Helper;

namespace PerformSDAPIDataIngest2016
{
    public class IngestTournamentCalendar : IngestionBase
    {
        private string performUrl = System.Configuration.ConfigurationManager.AppSettings["performUrlTC"];
        private string performUuId = System.Configuration.ConfigurationManager.AppSettings["performUrlUuId"];
        private StatsPerformHelper _performHelper;
        public IngestTournamentCalendar()
        {
            _performHelper = new StatsPerformHelper();
        }
        public void DoIngest()
        {
            logInfo("Starting ingestion...");

            

            string dataUrl = performUrl + "/{0}?_fmt=json&_rt=b";
            dataUrl = String.Format(dataUrl, performUuId);
            IngestUrl(dataUrl);

            logInfo("Completed ingestion.");
        }

        public void IngestUrl(string url)
        {
            var dm = new DatabaseManager();
            
            var tDate = DateTime.MinValue;

            try
            {
                var td = ReadcompListFromUrl(url);
                if (td != null)
                {
                    var competitionList = ConfigurationManager.GetSection("tournamentcalendar") as NameValueCollection;
                    if (competitionList != null)
                    {
                        foreach (var comp in competitionList.AllKeys)
                        {
                            string competionId = competitionList.GetValues(comp).FirstOrDefault();
                            var compCalendar = (from obj in td.competition
                                               where obj.id == competionId
                                               select obj).ToList();

                            if (compCalendar != null && compCalendar.Count > 0)
                            {
                                foreach (var c in compCalendar)
                                {
                                    try
                                    {
                                        string compId = ProcessCompetition(dm, c);

                                        //insert ot update country
                                        ProcessCountry(dm, c);

                                        ProcessCalendar(dm, c, compId);
                                    }
                                    catch (Exception ex)
                                    {
                                        logError(ex.StackTrace);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var compList = td.competition;
                        if (compList != null && compList.Count > 0)
                        {
                            foreach (var c in compList)
                            {
                                try
                                {
                                    //insert or update competition
                                    string compId = ProcessCompetition(dm, c);

                                    //insert ot update country
                                    ProcessCountry(dm, c);

                                    ProcessCalendar(dm, c, compId);
                                }
                                catch (Exception ex)
                                {
                                    logError(ex.StackTrace);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logError(ex.StackTrace);
            }
        }

        private void ProcessCalendar(DatabaseManager dm, JsonTCData.Competition c, string compId)
        {
            var tInt = 0;
            var tDate = DateTime.MinValue;
            var tourCldrList = c.tournamentCalendar;
            if (tourCldrList.Count > 0)
            {
                foreach (var r in tourCldrList)
                {
                    try
                    {
                        //insert or update competition
                     
                        var tcc = new TableTournamentCalendar();
                        tcc.id = r.id;
                        tcc.ocId = int.TryParse(r.ocId, out tInt) ? tInt : 0;
                        tcc.name = r.name;
                        tcc.startDate = DateTime.TryParse(r.startDate, out tDate) ? tDate : DateTime.MinValue;
                        tcc.endDate = DateTime.TryParse(r.endDate, out tDate) ? tDate : DateTime.MinValue;
                        tcc.active = r.active;
                        tcc.lastUpdated = DateTime.TryParse(r.lastUpdated, out tDate) ? tDate : DateTime.MinValue;
                        tcc.competitionId = compId;
                        var tcId = dm.InsertOrUpdateTournamentCalendar(tcc);
                    }
                    catch (Exception ex)
                    {
                        logError(ex.StackTrace);
                    }
                }
            }
        }

        private  void ProcessCountry(DatabaseManager dm, JsonTCData.Competition c)
        {
            var tco = new TableCountry();
            tco.id = c.countryId;
            tco.name = c.country;
            var cId = dm.InsertOrUpdateCountry(tco);
        }

        private  string ProcessCompetition(DatabaseManager dm, JsonTCData.Competition c)
        {
            
            logInfo("##########################################"+Environment.NewLine+"Updating calendar for:" + c.name);
            var tempInt = 0;
            //insert or update competition
            var tc = new TableCompetition();
            tc.id = c.id;
            tc.ocId = int.TryParse(c.ocId, out tempInt) ? tempInt : 0;
            tc.opId = int.TryParse(c.opId, out tempInt) ? tempInt : 0;
            tc.countryId = c.countryId;
            tc.name = c.name;
            var compId = dm.InsertOrUpdateCompetition(tc);
            return compId;
        }

        public JsonTCData ReadcompListFromUrl(string url)
        {
            // Create a list of articles to loop through later.
            var compList = new JsonTCData();

            try
            {
                // Store the response from the Url into a string for parsing.
                string htmlResponse = _performHelper.GetHtmlResponseFromUrl(url);
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                compList = js.Deserialize<JsonTCData>(htmlResponse);       
            }
            catch (Exception ex)
            {
                logError(ex.Message);
                return compList;
            }
            return compList;
        }

        #region Helper

    


        //public string GetHtmlResponseFromUrl(string url)
        //{
        //    logInfo("Reading from url : " + url);
        //    StringBuilder sb = new StringBuilder();
        //    byte[] buffer = new byte[8192];

        //    try
        //    {
        //        Uri uri = new Uri(url);

        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
        //        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //        Stream respStream = response.GetResponseStream();

        //        string tempString = "";
        //        int count = 0;

        //        do
        //        {
        //            count = respStream.Read(buffer, 0, buffer.Length);

        //            if (count != 0)
        //            {
        //                tempString = Encoding.UTF8.GetString(buffer, 0, count);
        //                sb.Append(tempString);
        //            }

        //        } while (count > 0);
        //    }
        //    catch (Exception ex)
        //    {
                
        //        throw ex;
        //    }
        //    return sb.ToString();
        //}
        #endregion
    }
}
