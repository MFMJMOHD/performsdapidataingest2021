﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016.Helper
{
    public class StatsPerformHelper:IngestionBase
    {
        PagerDuty.PagerDutyCaller _pagerDutyCaller;
        public StatsPerformHelper()
        {
            _pagerDutyCaller = new PagerDuty.PagerDutyCaller();
        }
        public string GetHtmlResponseFromUrl(string url)
        {
            logInfo("Reading from url : " + url);
            StringBuilder sb = new StringBuilder();
            byte[] buffer = new byte[8192];
            string pagerDutyErrorMessage;
            string callerMethod = (new System.Diagnostics.StackTrace()).GetFrame(1).GetMethod().Name;
            try
            {

                //url = "http://httpstat.us/500";
                //408 request timeout
                //403 Forbidden
                //400 badrequest

                Uri uri = new Uri(url);
                

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream respStream = response.GetResponseStream();

                string tempString = "";
                int count = 0;

                do
                {
                    count = respStream.Read(buffer, 0, buffer.Length);

                    if (count != 0)
                    {
                        tempString = Encoding.UTF8.GetString(buffer, 0, count);
                        sb.Append(tempString);
                    }

                } while (count > 0);
            }
            catch (WebException ex) when ((ex.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.InternalServerError||
            (ex.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.RequestTimeout||
            (ex.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.Forbidden||
            (ex.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.BadRequest||
            (ex.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.ServiceUnavailable)
            {
                pagerDutyErrorMessage = "Perform Soccer Result Ingestion.URL=" + url + " Caller method:"+callerMethod+" Exception:" + ex.Message;
                if(ex.InnerException!=null)
                    pagerDutyErrorMessage = "Perform Soccer Result Ingestion.URL=" + url + " Caller method:"+callerMethod+" Exception:" + ex.Message+
                        " Inner Exception:"+ex.InnerException.Message;
                _pagerDutyCaller.CallPagerDuty(pagerDutyErrorMessage);
                throw ex;
            }
            catch (WebException ex) when (ex.Status == WebExceptionStatus.NameResolutionFailure
            || ex.Status == WebExceptionStatus.ConnectFailure
            || ex.Status == WebExceptionStatus.Timeout)
            {
                pagerDutyErrorMessage = "Perform Soccer Result Ingestion.URL=" + url + " Caller method:" + callerMethod + " Exception:" + ex.Message;
                if (ex.InnerException != null)
                    pagerDutyErrorMessage = "Perform Soccer Result Ingestion.URL=" + url + " Caller method:" + callerMethod + " Exception:" + ex.Message +
                        " Inner Exception:" + ex.InnerException.Message;
                _pagerDutyCaller.CallPagerDuty(pagerDutyErrorMessage);
                throw ex;
            }
            //catch (WebException ex) when (ex.Status == WebExceptionStatus.ConnectFailure)
            //{
            //    _pagerDutyCaller.CallPagerDuty(ex.Message);
            //    return false;
            //}
            //catch (WebException ex) when (ex.Status == WebExceptionStatus.Timeout)
            //{
            //    _pagerDutyCaller.CallPagerDuty(ex.Message);
            //    return false;
            //}
            catch (Exception ex)
            {

                throw ex;
            }
            return sb.ToString();
        }

        
    }
}
