﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016
{
    public class JsonSquadData
    {
        public List<Squad> squad { get; set; }

        public class Person
        {
            public string id { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string lastName { get; set; }
            public string matchName { get; set; }
            public string nationalityId { get; set; }
            public string nationality { get; set; }
            public string position { get; set; }
            public int shirtNumber { get; set; }
            public string type { get; set; }
            public string dateOfBirth { get; set; }
            public string placeOfBirth { get; set; }
            public string countryOfBirthId { get; set; }
            public string countryOfBirth { get; set; }
            public int height { get; set; }
            public int weight { get; set; }
            public string foot { get; set; }
            public string status { get; set; }
        }

        public class Squad
        {
            public string contestantId { get; set; }
            public string contestantName { get; set; }
            public string competitionId { get; set; }
            public string competitionName { get; set; }
            public string contestantShortName { get; set; }
            public string contestantClubName { get; set; }
            public string contestantCode { get; set; }
            public string type { get; set; }
            public string teamType { get; set; }
            public List<Person> person { get; set; }
        }
    }
}
