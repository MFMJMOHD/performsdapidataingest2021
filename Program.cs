﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace PerformSDAPIDataIngest2016
{
    class Program
    {
        private static string enableTournamentCalendar = ConfigurationManager.AppSettings["enableTournamentCalendar"];
        private static string enableMatch = ConfigurationManager.AppSettings["enableMatch"];
        private static string enableMatchDetail = ConfigurationManager.AppSettings["enableMatchDetail"];
        private static string enableStanding = ConfigurationManager.AppSettings["enableStanding"];
        private static string enableSquad = ConfigurationManager.AppSettings["enableSquad"];
        private static string enablePersonCareer = ConfigurationManager.AppSettings["enablePersonCareer"];
        private static string enableRanking = ConfigurationManager.AppSettings["enableRanking"];
        private static string enableDateRange = ConfigurationManager.AppSettings["enableRanking"];
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            PerformIngestionConfig.Initialize();

            PagerDuty.PagerDutyCaller _pagerDutyCaller = new PagerDuty.PagerDutyCaller();
            DatabaseManager dm = new DatabaseManager();

            
            if (!TestDB(dm,_pagerDutyCaller))
                return;
        

            //validate the enable date value in setting

            if (enableTournamentCalendar == "yes")
            {
                var tc = new IngestTournamentCalendar();
                tc.DoIngest();
            }

            if (enableMatch == "yes")
            {
                var m = new IngestMatch();
                m.DoIngest();
            }

            if (enableMatchDetail == "yes")
            {
                var md = new IngestMatchDetail();
                md.DoIngest();
            }

            if (enableStanding == "yes")
            {
                var sd = new IngestStanding();
                sd.DoIngest();
            }

            if (enableSquad == "yes")
            {
                var pd = new IngestSquad();
                pd.DoIngest(enablePersonCareer);
            }

            if (enableRanking == "yes")
            {
                var rd = new IngestRanking();
                rd.DoIngest();
            }

            sw.Stop();
            Console.WriteLine(sw.Elapsed);
            
            //delay
            
        }

        static bool TestDB(DatabaseManager dm, PagerDuty.PagerDutyCaller _pagerDutyCaller)
        {
            string pagerDutyErrorMessage = string.Empty;
            try
            {
                dm.kendb.OpenSharedConnection();
            }
            catch (Exception ex)
            {
                pagerDutyErrorMessage = "Perform Soccer Result Ingestion. Exception:" + ex.Message;
                if (ex.InnerException != null)
                    pagerDutyErrorMessage = "Perform Soccer Result Ingestion.Exception:" + ex.Message +
                        " Inner Exception:" + ex.InnerException.Message;
                _pagerDutyCaller.CallPagerDuty(pagerDutyErrorMessage);
                return false;
            }
            finally
            {
                dm.kendb.CloseSharedConnection();
            }

            return true;
        }
    }
}
