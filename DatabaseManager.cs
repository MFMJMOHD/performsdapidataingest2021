﻿using PerformSDAPIDataIngest2016.PagerDuty;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016
{
    public class DatabaseManager : IngestionBase
    {
        private Random random = new Random();
        
        public DatabaseManager()
        {
            
        }
        #region Insert/Update
        public string InsertOrUpdateCountry(TableCountry data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommon_Country where id = @0", data.id);
            var result = kendb.Fetch<TableCountry>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_Country table : " + DateTime.Now);
                    result = new TableCountry();
                    result.id = data.id;
                    result.name = data.name;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Updating SDCommon_Country table : " + DateTime.Now);
                    //result.id = data.id;
                    result.name = string.IsNullOrEmpty(data.name) ? result.name : data.name;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.id));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateCompetition(TableCompetition data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommon_Competition where id = @0", data.id);
            var result = kendb.Fetch<TableCompetition>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_Competition table : " + DateTime.Now);
                    result = new TableCompetition();
                    result.id = data.id;
                    result.ocId = data.ocId;
                    result.opId = data.opId;
                    result.name = data.name;
                    result.countryId = data.countryId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Updating SDCommon_Competition table : " + DateTime.Now);
                    //result.id = data.id;
                    result.ocId = (data.ocId == 0) ? result.ocId : data.ocId;
                    result.opId = (data.opId == 0) ? result.opId : data.opId;
                    result.name = string.IsNullOrEmpty(data.name) ? result.name : data.name;
                    result.countryId = string.IsNullOrEmpty(data.countryId) ? result.countryId : data.countryId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.id));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateTournamentCalendar(TableTournamentCalendar data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommon_TournamentCalendar where id = @0", data.id);
            var result = kendb.Fetch<TableTournamentCalendar>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_TournamentCalendar table : " + DateTime.Now);
                    result = new TableTournamentCalendar();
                    result.id = data.id;
                    result.ocId = data.ocId;
                    result.name = data.name;
                    result.startDate = (data.startDate == DateTime.MinValue) ? null : data.startDate;
                    result.endDate = (data.endDate == DateTime.MinValue) ? null : data.endDate;
                    result.active = data.active;
                    result.lastUpdated = (data.lastUpdated == DateTime.MinValue) ? null : data.lastUpdated;
                    result.competitionId = data.competitionId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Updating SDCommon_TournamentCalendar table : " + DateTime.Now);
                    //result.id = data.id;
                    result.ocId = (data.ocId == 0) ? result.ocId : data.ocId;
                    result.name = string.IsNullOrEmpty(data.name) ? result.name : data.name;
                    result.startDate = (data.startDate == null || data.startDate == DateTime.MinValue) ? (result.startDate != null ? result.startDate : null) : data.startDate;
                    result.endDate = (data.endDate == null || data.endDate == DateTime.MinValue) ? (result.endDate != null ? result.endDate : null) : data.endDate;
                    result.active = string.IsNullOrEmpty(data.active) ? result.active : data.active;
                    result.lastUpdated = (data.lastUpdated == null || data.lastUpdated == DateTime.MinValue) ? (result.lastUpdated != null ? result.lastUpdated : null) : data.lastUpdated;
                    result.competitionId = string.IsNullOrEmpty(data.competitionId) ? result.competitionId : data.competitionId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.id));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateSport(TableSport data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommon_Sport where id = @0", data.id);
            var result = kendb.Fetch<TableSport>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_Sport table : " + DateTime.Now);
                    result = new TableSport();
                    result.id = data.id;
                    result.name = data.name;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Updating SDCommon_Sport table : " + DateTime.Now);
                    //result.id = data.id;
                    result.name = string.IsNullOrEmpty(data.name) ? result.name : data.name;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.id));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateRuleSet(TableRuleset data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommon_RuleSet where id = @0", data.id);
            var result = kendb.Fetch<TableRuleset>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_RuleSet table : " + DateTime.Now);
                    result = new TableRuleset();
                    result.id = data.id;
                    result.name = data.name;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Updating SDCommon_RuleSet table : " + DateTime.Now);
                    //result.id = data.id;
                    result.name = string.IsNullOrEmpty(data.name) ? result.name : data.name;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.id));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateContestant(TableContestant data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommon_Contestant where id = @0", data.id);
            var result = kendb.Fetch<TableContestant>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_Contestant table : " + DateTime.Now);
                    result = new TableContestant();
                    result.id = data.id;
                    result.teamType = data.teamType;
                    result.name = data.name;
                    result.shortName = data.shortName;
                    result.clubName = data.clubName;
                    result.code = data.code;
                    result.countryId = data.countryId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Updating SDCommon_Contestant table : " + DateTime.Now);
                    //result.id = data.id;
                    result.teamType = string.IsNullOrEmpty(data.teamType) ? result.teamType : data.teamType;
                    result.name = string.IsNullOrEmpty(data.name) ? result.name : data.name;
                    result.shortName = string.IsNullOrEmpty(data.shortName) ? result.shortName : data.shortName;
                    result.clubName = string.IsNullOrEmpty(data.clubName) ? result.clubName : data.clubName;
                    result.code = string.IsNullOrEmpty(data.code) ? result.code : data.code;
                    result.countryId = string.IsNullOrEmpty(data.countryId) ? result.countryId : data.countryId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.id));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateStage(TableStage data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommon_Stage where id = @0", data.id);
            var result = kendb.Fetch<TableStage>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_Stage table : " + DateTime.Now);
                    result = new TableStage();
                    result.id = data.id;
                    result.name = data.name;
                    result.startDate = (data.startDate == DateTime.MinValue) ? null : data.startDate;
                    result.endDate = (data.endDate == DateTime.MinValue) ? null : data.endDate;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Updating SDCommon_Stage table : " + DateTime.Now);
                    //result.id = data.id;
                    result.name = string.IsNullOrEmpty(data.name) ? result.name : data.name;
                    result.startDate = (data.startDate == null || data.startDate == DateTime.MinValue) ? (result.startDate != null ? result.startDate : null) : data.startDate;
                    result.endDate = (data.endDate == null || data.endDate == DateTime.MinValue) ? (result.endDate != null ? result.endDate : null) : data.endDate;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.id));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateSeries(TableSeries data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommon_Series where id = @0", data.id);
            var result = kendb.Fetch<TableSeries>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_Series table : " + DateTime.Now);
                    result = new TableSeries();
                    result.id = data.id;
                    result.name = data.name;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Updating SDCommon_Series table : " + DateTime.Now);
                    //result.id = data.id;
                    result.name = string.IsNullOrEmpty(data.name) ? result.name : data.name;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.id));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateVenue(TableVenue data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommon_Venue where id = @0", data.id);
            var result = kendb.Fetch<TableVenue>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_Venue table : " + DateTime.Now);
                    result = new TableVenue();
                    result.id = data.id;
                    result.name = data.name;
                    result.longName = data.longName;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Updating SDCommon_Venue table : " + DateTime.Now);
                    //result.id = data.id;
                    result.name = string.IsNullOrEmpty(data.name) ? result.name : data.name;
                    result.longName = string.IsNullOrEmpty(data.longName) ? result.longName : data.longName;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.id));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateMatch(TableMatchInfo data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommon_Match where id = @0", data.id);
            var result = kendb.Fetch<TableMatchInfo>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_Match table : " + DateTime.Now);
                    result = new TableMatchInfo();
                    result.id = data.id;
                    result.sportId = data.sportId;
                    result.ruleSetId = data.ruleSetId;
                    result.description = data.description;
                    result.teamAId = data.teamAId;
                    result.teamBId = data.teamBId;
                    result.date = (data.date == DateTime.MinValue) ? null : data.date;
                    result.week = data.week;
                    result.competitionId = data.competitionId;
                    result.tcalendarId = data.tcalendarId;
                    result.stageId = data.stageId;
                    result.seriesId = data.seriesId;
                    result.venueId = data.venueId;
                    result.lastUpdated = (data.lastUpdated == DateTime.MinValue) ? null : data.lastUpdated;
                    //match details
                    result.matchStatus = data.matchStatus;
                    result.periodId = data.periodId;
                    result.periodCount = data.periodCount;
                    result.winner = data.winner;
                    result.matchLengthMin = data.matchLengthMin;
                    result.matchLengthSec = data.matchLengthSec;
                    result.scoreHtHome = data.scoreHtHome;
                    result.scoreHtAway = data.scoreHtAway;
                    result.scoreFtHome = data.scoreFtHome;
                    result.scoreFtAway = data.scoreFtAway;
                    result.scoreEtHome = data.scoreEtHome;
                    result.scoreEtAway = data.scoreEtAway;
                    result.scorePenHome = data.scorePenHome;
                    result.scorePenAway = data.scorePenAway;
                    result.scoreTotHome = data.scoreTotHome;
                    result.scoreTotAway = data.scoreTotAway;
                    result.scoreAggHome = data.scoreAggHome;
                    result.scoreAggAway = data.scoreAggAway;
                    result.aggregateWinnerId = data.aggregateWinnerId;
                    //match details extra
                    result.attendance = data.attendance;
                    result.weather = data.weather;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Updating SDCommon_Match table : " + DateTime.Now);
                    //result.id = data.id;
                    result.sportId = string.IsNullOrEmpty(data.sportId) ? result.sportId : data.sportId;
                    result.ruleSetId = string.IsNullOrEmpty(data.ruleSetId) ? result.ruleSetId : data.ruleSetId;
                    result.description = string.IsNullOrEmpty(data.description) ? result.description : data.description;
                    result.teamAId = string.IsNullOrEmpty(data.teamAId) ? result.teamAId : data.teamAId;
                    result.teamBId = string.IsNullOrEmpty(data.teamBId) ? result.teamBId : data.teamBId;
                    result.date = (data.date == null || data.date == DateTime.MinValue) ? (result.date != null ? result.date : null) : data.date;
                    result.week = string.IsNullOrEmpty(data.week) ? result.week : data.week;
                    result.competitionId = string.IsNullOrEmpty(data.competitionId) ? result.competitionId : data.competitionId;
                    result.tcalendarId = string.IsNullOrEmpty(data.tcalendarId) ? result.tcalendarId : data.tcalendarId;
                    result.stageId = string.IsNullOrEmpty(data.stageId) ? result.stageId : data.stageId;
                    result.seriesId = string.IsNullOrEmpty(data.seriesId) ? result.seriesId : data.seriesId;
                    result.venueId = string.IsNullOrEmpty(data.venueId) ? result.venueId : data.venueId;
                    result.lastUpdated = (data.lastUpdated == null || data.lastUpdated == DateTime.MinValue) ? (result.lastUpdated != null ? result.lastUpdated : null) : data.lastUpdated;
                    //match details
                    //The match status : Fixture, Playing, Played, Cancelled, Postponed, Suspended
                    result.matchStatus = string.IsNullOrEmpty(data.matchStatus) ? result.matchStatus : data.matchStatus;
                    result.periodId = data.periodId == 0 ? result.periodId : data.periodId;
                    result.periodCount = data.periodCount == 0 ? result.periodCount : data.periodCount;
                    result.winner = string.IsNullOrEmpty(data.winner) ? result.winner : data.winner;
                    result.matchLengthMin = data.matchLengthMin == 0 ? result.matchLengthMin : data.matchLengthMin;
                    result.matchLengthSec = data.matchLengthSec == 0 ? result.matchLengthSec : data.matchLengthSec;
                    result.scoreHtHome = data.scoreHtHome == 0 ? result.scoreHtHome : data.scoreHtHome;
                    result.scoreHtAway = data.scoreHtAway == 0 ? result.scoreHtAway : data.scoreHtAway;
                    result.scoreFtHome = data.scoreFtHome == 0 ? result.scoreFtHome : data.scoreFtHome;
                    result.scoreFtAway = data.scoreFtAway == 0 ? result.scoreFtAway : data.scoreFtAway;
                    result.scoreEtHome = data.scoreEtHome == 0 ? result.scoreEtHome : data.scoreEtHome;
                    result.scoreEtAway = data.scoreEtAway == 0 ? result.scoreEtAway : data.scoreEtAway;
                    result.scorePenHome = data.scorePenHome == 0 ? result.scorePenHome : data.scorePenHome;
                    result.scorePenAway = data.scorePenAway == 0 ? result.scorePenAway : data.scorePenAway;
                    result.scoreTotHome = data.scoreTotHome == 0 ? result.scoreTotHome : data.scoreTotHome;
                    result.scoreTotAway = data.scoreTotAway == 0 ? result.scoreTotAway : data.scoreTotAway;
                    result.scoreAggHome = data.scoreAggHome == 0 ? result.scoreAggHome : data.scoreAggHome;
                    result.scoreAggAway = data.scoreAggAway == 0 ? result.scoreAggAway : data.scoreAggAway;
                    result.aggregateWinnerId = string.IsNullOrEmpty(data.aggregateWinnerId) ? result.aggregateWinnerId : data.aggregateWinnerId;
                    //match details extra
                    result.attendance = data.attendance == 0 ? result.attendance : data.attendance;
                    result.weather = string.IsNullOrEmpty(data.weather) ? result.weather : data.weather;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.id));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public long InsertOrUpdateMatchGoals(TableMatchGoals data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommonMatch_Goals where playerId=@0 and type=@1 and timeMin=@2 and timeMinPlus=@3 and matchId=@4", data.playerId, data.type, data.timeMin, data.timeMinPlus, data.matchId);
            var result = kendb.Fetch<TableMatchGoals>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_Goals table : " + DateTime.Now);
                    result = new TableMatchGoals();
                    result.id = data.id;
                    result.optaEventId = data.optaEventId;
                    result.type = data.type;
                    result.playerId = data.playerId;
                    result.contestantId = data.contestantId;
                    result.timeMin = data.timeMin;
                    result.timeMinPlus = data.timeMinPlus;
                    result.periodId = data.periodId;
                    result.lastUpdated = (data.lastUpdated == DateTime.MinValue) ? null : data.lastUpdated;
                    result.matchId = data.matchId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, result.playerId, result.type, result.timeMin, result.timeMinPlus, result.matchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_Goals table : " + DateTime.Now);
                    //result.Id = data.Id;
                    result.optaEventId = (data.optaEventId == 0) ? result.optaEventId : data.optaEventId;
                    result.type = string.IsNullOrEmpty(data.type) ? result.type : data.type;
                    result.playerId = string.IsNullOrEmpty(data.playerId) ? result.playerId : data.playerId;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    result.lastUpdated = (data.lastUpdated == null || data.lastUpdated == DateTime.MinValue) ? (result.lastUpdated != null ? result.lastUpdated : null) : data.lastUpdated;
                    result.timeMin = data.timeMin == 0 ? result.timeMin : data.timeMin;
                    result.timeMinPlus = data.timeMinPlus == 0 ? result.timeMinPlus : data.timeMinPlus;
                    result.periodId = data.periodId == 0 ? result.periodId : data.periodId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, result.playerId, result.type, result.timeMin, result.timeMinPlus, result.matchId));
                }

                return result.optaEventId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public long InsertOrUpdateMatchAssists(TableMatchAssists data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommonMatch_Assists where playerId=@0 and type=@1 and timeMin=@2 and timeMinPlus=@3 and matchId=@4", data.playerId, data.type, data.timeMin, data.timeMinPlus, data.matchId);
            var result = kendb.Fetch<TableMatchAssists>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_Assists table : " + DateTime.Now);
                    result = new TableMatchAssists();
                    result.id = data.id;
                    result.optaEventId = data.optaEventId;
                    result.type = data.type;
                    result.playerId = data.playerId;
                    result.contestantId = data.contestantId;
                    result.timeMin = data.timeMin;
                    result.timeMinPlus = data.timeMinPlus;
                    result.periodId = data.periodId;
                    result.lastUpdated = (data.lastUpdated == DateTime.MinValue) ? null : data.lastUpdated;
                    result.matchId = data.matchId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, result.playerId, result.type, result.timeMin, result.timeMinPlus, result.matchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_Assists table : " + DateTime.Now);
                    //result.Id = data.Id;
                    result.optaEventId = (data.optaEventId == 0) ? result.optaEventId : data.optaEventId;
                    result.type = string.IsNullOrEmpty(data.type) ? result.type : data.type;
                    result.playerId = string.IsNullOrEmpty(data.playerId) ? result.playerId : data.playerId;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    result.lastUpdated = (data.lastUpdated == null || data.lastUpdated == DateTime.MinValue) ? (result.lastUpdated != null ? result.lastUpdated : null) : data.lastUpdated;
                    result.timeMin = data.timeMin == 0 ? result.timeMin : data.timeMin;
                    result.timeMinPlus = data.timeMinPlus == 0 ? result.timeMinPlus : data.timeMinPlus;
                    result.periodId = data.periodId == 0 ? result.periodId : data.periodId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, result.playerId, result.type, result.timeMin, result.timeMinPlus, result.matchId));
                }

                return result.optaEventId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public long InsertOrUpdateMatchPenaltyShot(TableMatchPenaltyShot data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommonMatch_PenaltyShot where playerId=@0 and teamPenaltyNumber=@1 and matchId=@2", data.playerId, data.teamPenaltyNumber, data.matchId);
            var result = kendb.Fetch<TableMatchPenaltyShot>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_PenaltyShot table : " + DateTime.Now);
                    result = new TableMatchPenaltyShot();
                    result.id = data.id;
                    result.optaEventId = data.optaEventId;
                    result.playerId = data.playerId;
                    result.contestantId = data.contestantId;
                    result.outcome = data.outcome;
                    result.teamPenaltyNumber = data.teamPenaltyNumber;
                    result.lastUpdated = (data.lastUpdated == DateTime.MinValue) ? null : data.lastUpdated;
                    result.matchId = data.matchId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2} : {3}", DateTime.Now, result.playerId, result.teamPenaltyNumber, result.matchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_PenaltyShot table : " + DateTime.Now);
                    //result.Id = data.Id;
                    result.optaEventId = (data.optaEventId == 0) ? result.optaEventId : data.optaEventId;
                    result.playerId = string.IsNullOrEmpty(data.playerId) ? result.playerId : data.playerId;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    result.lastUpdated = (data.lastUpdated == null || data.lastUpdated == DateTime.MinValue) ? (result.lastUpdated != null ? result.lastUpdated : null) : data.lastUpdated;
                    result.outcome = string.IsNullOrEmpty(data.outcome) ? result.outcome : data.outcome;
                    result.teamPenaltyNumber = data.teamPenaltyNumber == 0 ? result.teamPenaltyNumber : data.teamPenaltyNumber;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2} : {3}", DateTime.Now, result.playerId, result.teamPenaltyNumber, result.matchId));
                }

                return result.optaEventId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public long InsertOrUpdateMatchBookings(TableMatchBookings data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommonMatch_Bookings where playerId=@0 and type=@1 and timeMin=@2 and timeMinPlus=@3 and matchId=@4", data.playerId, data.type, data.timeMin, data.timeMinPlus, data.matchId);
            var result = kendb.Fetch<TableMatchBookings>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_Bookings table : " + DateTime.Now);
                    result = new TableMatchBookings();
                    result.id = data.id;
                    result.optaEventId = data.optaEventId;
                    result.type = data.type;
                    result.playerId = data.playerId;
                    result.contestantId = data.contestantId;
                    result.timeMin = data.timeMin;
                    result.timeMinPlus = data.timeMinPlus;
                    result.periodId = data.periodId;
                    result.lastUpdated = (data.lastUpdated == DateTime.MinValue) ? null : data.lastUpdated;
                    result.matchId = data.matchId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, result.playerId, result.type, result.timeMin, result.timeMinPlus, result.matchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_Bookings table : " + DateTime.Now);
                    //result.Id = data.Id;
                    result.optaEventId = (data.optaEventId == 0) ? result.optaEventId : data.optaEventId;
                    result.type = string.IsNullOrEmpty(data.type) ? result.type : data.type;
                    result.playerId = string.IsNullOrEmpty(data.playerId) ? result.playerId : data.playerId;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    result.lastUpdated = (data.lastUpdated == null || data.lastUpdated == DateTime.MinValue) ? (result.lastUpdated != null ? result.lastUpdated : null) : data.lastUpdated;
                    result.timeMin = data.timeMin == 0 ? result.timeMin : data.timeMin;
                    result.timeMinPlus = data.timeMinPlus == 0 ? result.timeMinPlus : data.timeMinPlus;
                    result.periodId = data.periodId == 0 ? result.periodId : data.periodId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, result.playerId, result.type, result.timeMin, result.timeMinPlus, result.matchId));
                }

                return result.optaEventId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public long InsertOrUpdateMatchSubstitutions(TableMatchSubstitutions data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommonMatch_Substitutions where playerOnId=@0 and playerOffId=@1 and contestantId=@2 and matchId=@3", data.playerOnId, data.playerOffId, data.contestantId, data.matchId);
            var result = kendb.Fetch<TableMatchSubstitutions>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_Substitutions table : " + DateTime.Now);
                    result = new TableMatchSubstitutions();
                    result.id = data.id;
                    result.optaEventId = data.optaEventId;
                    result.playerOnId = data.playerOnId;
                    result.playerOffId = data.playerOffId;
                    result.contestantId = data.contestantId;
                    result.timeMin = data.timeMin;
                    result.timeMinPlus = data.timeMinPlus;
                    result.periodId = data.periodId;
                    result.lastUpdated = (data.lastUpdated == DateTime.MinValue) ? null : data.lastUpdated;
                    result.matchId = data.matchId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2} : {3} : {4}", DateTime.Now, result.playerOnId, result.playerOffId, result.contestantId, result.matchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_Substitutions table : " + DateTime.Now);
                    //result.id = data.id;
                    result.optaEventId = (data.optaEventId == 0) ? result.optaEventId : data.optaEventId;
                    result.playerOnId = string.IsNullOrEmpty(data.playerOnId) ? result.playerOnId : data.playerOnId;
                    result.playerOffId = string.IsNullOrEmpty(data.playerOffId) ? result.playerOffId : data.playerOffId;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.timeMin = data.timeMin == 0 ? result.timeMin : data.timeMin;
                    result.timeMinPlus = data.timeMinPlus == 0 ? result.timeMinPlus : data.timeMinPlus;
                    result.periodId = data.periodId == 0 ? result.periodId : data.periodId;
                    result.lastUpdated = (data.lastUpdated == null || data.lastUpdated == DateTime.MinValue) ? (result.lastUpdated != null ? result.lastUpdated : null) : data.lastUpdated;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2} : {3} : {4}", DateTime.Now, result.playerOnId, result.playerOffId, result.contestantId, result.matchId));
                }

                return result.optaEventId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public long InsertOrUpdateMatchLineups(TableMatchLineups data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommonMatch_Lineups where playerId=@0 and matchId=@1", data.playerId, data.matchId);
            var result = kendb.Fetch<TableMatchLineups>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_Lineups table : " + DateTime.Now);
                    result = new TableMatchLineups();
                    result.id = data.id;
                    result.optaEventId = data.optaEventId;
                    result.playerId = data.playerId;
                    result.shirtNumber = data.shirtNumber;
                    result.position = data.position;
                    result.positionSide = data.positionSide;
                    result.contestantId = data.contestantId;
                    result.matchId = data.matchId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2}", DateTime.Now, result.playerId, result.matchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_Lineups table : " + DateTime.Now);
                    //result.Id = data.Id;
                    result.optaEventId = (data.optaEventId == 0) ? result.optaEventId : data.optaEventId;
                    result.playerId = string.IsNullOrEmpty(data.playerId) ? result.playerId : data.playerId;
                    result.shirtNumber = (data.shirtNumber == 0) ? result.shirtNumber : data.shirtNumber;
                    result.position = string.IsNullOrEmpty(data.position) ? result.position : data.position;
                    result.positionSide = string.IsNullOrEmpty(data.positionSide) ? result.positionSide : data.positionSide;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2}", DateTime.Now, result.playerId, result.matchId));
                }

                return result.optaEventId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public long InsertOrUpdateMatchOfficials(TableMatchOfficials data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommonMatch_Officials where officialId=@0 and matchId=@1", data.officialId, data.matchId);
            var result = kendb.Fetch<TableMatchOfficials>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_Officials table : " + DateTime.Now);
                    result = new TableMatchOfficials();
                    result.id = data.id;
                    result.optaEventId = data.optaEventId;
                    result.officialId = data.officialId;
                    result.type = data.type;
                    result.firstName = data.firstName;
                    result.lastName = data.lastName;
                    result.matchId = data.matchId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2}", DateTime.Now, result.officialId, result.matchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_Officials table : " + DateTime.Now);
                    //result.Id = data.Id;
                    result.optaEventId = (data.optaEventId == 0) ? result.optaEventId : data.optaEventId;
                    result.officialId = string.IsNullOrEmpty(data.officialId) ? result.officialId : data.officialId;
                    result.type = string.IsNullOrEmpty(data.type) ? result.type : data.type;
                    result.firstName = string.IsNullOrEmpty(data.firstName) ? result.firstName : data.firstName;
                    result.lastName = string.IsNullOrEmpty(data.lastName) ? result.lastName : data.lastName;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2}", DateTime.Now, result.officialId, result.matchId));
                }

                return result.optaEventId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public long InsertOrUpdateMatchTeamStats(TableMatchTeamStats data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommonMatch_TeamStats where type=@0 and contestantId=@1 and matchId=@2", data.type, data.contestantId, data.matchId);
            var result = kendb.Fetch<TableMatchTeamStats>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_TeamStats table : " + DateTime.Now);
                    result = new TableMatchTeamStats();
                    result.id = data.id;
                    result.optaEventId = data.optaEventId;
                    result.fh = data.fh;
                    result.sh = data.sh;
                    result.type = data.type;
                    result.value = data.value;
                    result.contestantId = data.contestantId;
                    result.matchId = data.matchId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2} : {3}", DateTime.Now, result.type, result.contestantId, result.matchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_TeamStats table : " + DateTime.Now);
                    //result.id = data.id;
                    result.optaEventId = (data.optaEventId == 0) ? result.optaEventId : data.optaEventId;
                    result.fh = (data.fh == 0) ? result.fh : data.fh;
                    result.sh = (data.sh == 0) ? result.sh : data.sh;
                    result.type = string.IsNullOrEmpty(data.type) ? result.type : data.type;
                    result.value = (data.value == 0) ? result.value : data.value;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2} : {3}", DateTime.Now, result.type, result.contestantId, result.matchId));
                }

                return result.optaEventId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public long InsertOrUpdateMatchPlayerStats(TableMatchPlayerStats data)
        {
            var sql = Sql.Builder.Append(@"select * from SDCommonMatch_PlayerStats where type=@0 and playerId=@1 and matchId=@2", data.type, data.playerId, data.matchId);
            var result = kendb.Fetch<TableMatchPlayerStats>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_PlayerStats table : " + DateTime.Now);
                    result = new TableMatchPlayerStats();
                    result.id = data.id;
                    result.optaEventId = data.optaEventId;
                    result.type = data.type;
                    result.value = data.value;
                    result.playerId = data.playerId;
                    result.matchId = data.matchId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2} : {3}", DateTime.Now, result.type, result.playerId, result.matchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_PlayerStats table : " + DateTime.Now);
                    //result.Id = data.Id;
                    result.optaEventId = (data.optaEventId == 0) ? result.optaEventId : data.optaEventId;
                    result.type = string.IsNullOrEmpty(data.type) ? result.type : data.type;
                    result.value = (data.value == 0) ? result.value : data.value;
                    result.playerId = string.IsNullOrEmpty(data.playerId) ? result.playerId : data.playerId;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2} : {3}", DateTime.Now, result.type, result.playerId, result.matchId));
                }

                return result.optaEventId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public int InsertOrUpdateStanding(TableStanding data)
        {
            var sql = Sql.Builder;

            if (data.seriesId != null)
                sql = Sql.Builder.Append(@"select * from SDCommon_Standing where type=@0 and rank=@1 and stageId=@2 and seriesId=@3 and tcalendarId=@4", data.type, data.rank, data.stageId, data.seriesId, data.tcalendarId);
            else
                sql = Sql.Builder.Append(@"select * from SDCommon_Standing where type=@0 and rank=@1 and stageId=@2 and tcalendarId=@3", data.type, data.rank, data.stageId, data.tcalendarId);

            var result = kendb.Fetch<TableStanding>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_Standing table : " + DateTime.Now);
                    result = new TableStanding();
                    result.id = data.id;
                    result.rank = data.rank;
                    result.lastRank = data.lastRank;
                    result.contestantId = data.contestantId;
                    result.matchesPlayed = data.matchesPlayed;
                    result.matchesWon = data.matchesWon;
                    result.matchesLost = data.matchesLost;
                    result.matchesDrawn = data.matchesDrawn;
                    result.goalsFor = data.goalsFor;
                    result.goalsAgainst = data.goalsAgainst;
                    result.goaldifference = data.goaldifference;
                    result.points = data.points;
                    result.type = data.type;
                    result.seriesId = data.seriesId;
                    result.stageId = data.stageId;
                    result.tcalendarId = data.tcalendarId;
                    result.competitionId = data.competitionId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, result.type, result.rank, result.stageId, result.seriesId, result.tcalendarId));
                }
                else
                {
                    logDebug("Updating SDCommon_Standing table : " + DateTime.Now);
                    //result.id = data.id;
                    result.rank = (data.rank < 0) ? result.rank : data.rank;
                    result.lastRank = (data.lastRank < 0) ? result.lastRank : data.lastRank;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.matchesPlayed = (data.matchesPlayed < 0) ? result.matchesPlayed : data.matchesPlayed;
                    result.matchesWon = (data.matchesWon < 0) ? result.matchesWon : data.matchesWon;
                    result.matchesLost = (data.matchesLost < 0) ? result.matchesLost : data.matchesLost;
                    result.matchesDrawn = (data.matchesDrawn < 0) ? result.matchesDrawn : data.matchesDrawn;
                    result.goalsFor = (data.goalsFor < 0) ? result.goalsFor : data.goalsFor;
                    result.goalsAgainst = (data.goalsAgainst < 0) ? result.goalsAgainst : data.goalsAgainst;
                    result.goaldifference = string.IsNullOrEmpty(data.goaldifference) ? result.goaldifference : data.goaldifference;
                    result.points = (data.points < 0) ? result.points : data.points;
                    result.type = string.IsNullOrEmpty(data.type) ? result.type : data.type;
                    result.seriesId = string.IsNullOrEmpty(data.seriesId) ? result.seriesId : data.seriesId;
                    result.stageId = string.IsNullOrEmpty(data.stageId) ? result.stageId : data.stageId;
                    result.tcalendarId = string.IsNullOrEmpty(data.tcalendarId) ? result.tcalendarId : data.tcalendarId;
                    result.competitionId = string.IsNullOrEmpty(data.competitionId) ? result.competitionId : data.competitionId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, result.type, result.rank, result.stageId, result.seriesId, result.tcalendarId));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdatePerson(TablePerson data)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommon_Person where id=@0", data.id);

            var result = kendb.Fetch<TablePerson>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommon_Person table : " + DateTime.Now);
                    result = new TablePerson();
                    result.id = data.id;
                    result.type = data.type;
                    result.firstName = data.firstName;
                    result.middleName = data.middleName;
                    result.lastName = data.lastName;
                    result.matchName = data.matchName;
                    result.dateOfBirth = (data.dateOfBirth == DateTime.MinValue) ? null : data.dateOfBirth;
                    result.placeOfBirth = data.placeOfBirth;
                    result.countryOfBirthId = data.countryOfBirthId;
                    result.countryOfBirth = data.countryOfBirth;
                    result.height = data.height;
                    result.weight = data.weight;
                    result.foot = data.foot;
                    result.status = data.status;
                    result.nationalityId = data.nationalityId;
                    result.position = data.position;
                    result.shirtNumber = data.shirtNumber;
                    result.lastUpdated = (data.lastUpdated == DateTime.MinValue) ? null : data.lastUpdated;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Updating SDCommon_Person table : " + DateTime.Now);
                    //result.id = data.id;
                    result.type = string.IsNullOrEmpty(data.type) ? result.type : data.type;
                    result.firstName = string.IsNullOrEmpty(data.firstName) ? result.firstName : data.firstName;
                    result.middleName = string.IsNullOrEmpty(data.middleName) ? result.middleName : data.middleName;
                    result.lastName = string.IsNullOrEmpty(data.lastName) ? result.lastName : data.lastName;
                    result.matchName = string.IsNullOrEmpty(data.matchName) ? result.matchName : data.matchName;
                    result.dateOfBirth = (data.dateOfBirth == null || data.dateOfBirth == DateTime.MinValue) ? (result.dateOfBirth != null ? result.dateOfBirth : null) : data.dateOfBirth;
                    result.placeOfBirth = string.IsNullOrEmpty(data.placeOfBirth) ? result.placeOfBirth : data.placeOfBirth;
                    result.countryOfBirthId = string.IsNullOrEmpty(data.countryOfBirthId) ? result.countryOfBirthId : data.countryOfBirthId;
                    result.countryOfBirth = string.IsNullOrEmpty(data.countryOfBirth) ? result.countryOfBirth : data.countryOfBirth;
                    result.height = (data.height == 0) ? result.height : data.height;
                    result.weight = (data.weight == 0) ? result.weight : data.weight;
                    result.foot = string.IsNullOrEmpty(data.foot) ? result.foot : data.foot;
                    result.status = string.IsNullOrEmpty(data.status) ? result.status : data.status;
                    result.nationalityId = string.IsNullOrEmpty(data.nationalityId) ? result.nationalityId : data.nationalityId;
                    result.position = string.IsNullOrEmpty(data.position) ? result.position : data.position;
                    result.shirtNumber = (data.shirtNumber == 0) ? result.shirtNumber : data.shirtNumber;
                    result.lastUpdated = (data.lastUpdated == null || data.lastUpdated == DateTime.MinValue) ? (result.lastUpdated != null ? result.lastUpdated : null) : data.lastUpdated;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.id));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public int InsertOrUpdateContestantSquad(TableContestantSquad data)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommonContestant_Squad where playerId=@0 and tcalendarId=@1", data.playerId, data.tcalendarId);

            var result = kendb.Fetch<TableContestantSquad>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonContestant_Squad table : " + DateTime.Now);
                    result = new TableContestantSquad();
                    result.id = data.id;
                    result.contestantId = data.contestantId;
                    result.playerId = data.playerId;
                    result.tcalendarId = data.tcalendarId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2}", DateTime.Now, result.playerId, result.tcalendarId));
                }
                else
                {
                    logDebug("Updating SDCommonContestant_Squad table : " + DateTime.Now);
                    //result.id = data.id;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.playerId = string.IsNullOrEmpty(data.playerId) ? result.playerId : data.playerId;
                    result.tcalendarId = string.IsNullOrEmpty(data.tcalendarId) ? result.tcalendarId : data.tcalendarId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2}", DateTime.Now, result.playerId, result.tcalendarId));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public int InsertOrUpdatePersonCareer(TablePersonCareer data)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommonPerson_Career where contestantId=@0 and playerId=@1", data.contestantId, data.playerId);

            var result = kendb.Fetch<TablePersonCareer>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonPerson_Career table : " + DateTime.Now);
                    result = new TablePersonCareer();
                    result.id = data.id;
                    result.contestantId = data.contestantId;
                    result.contestantType = data.contestantType;
                    result.active = data.active;
                    result.startDate = (data.startDate == DateTime.MinValue) ? null : data.startDate;
                    result.endDate = (data.startDate == DateTime.MinValue) ? null : data.startDate;
                    result.role = data.role;
                    result.playerId = data.playerId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2}", DateTime.Now, result.contestantId, result.playerId));
                }
                else
                {
                    logDebug("Updating SDCommonPerson_Career table : " + DateTime.Now);
                    //result.id = data.id;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.contestantType = string.IsNullOrEmpty(data.contestantType) ? result.contestantType : data.contestantType;
                    result.active = string.IsNullOrEmpty(data.active) ? result.active : data.active;
                    result.startDate = (data.startDate == null || data.startDate == DateTime.MinValue) ? (result.startDate != null ? result.startDate : null) : data.startDate;
                    result.endDate = (data.endDate == null || data.endDate == DateTime.MinValue) ? (result.endDate != null ? result.endDate : null) : data.endDate;
                    result.role = string.IsNullOrEmpty(data.role) ? result.role : data.role;
                    result.playerId = string.IsNullOrEmpty(data.playerId) ? result.playerId : data.playerId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2}", DateTime.Now, result.contestantId, result.playerId));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public int InsertOrUpdatePersonCareerStats(TablePersonCareerStats data)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommonPerson_CareerStats where contestantId=@0 and playerId=@1 and competitionId=@2 and tournamentCalendarId=@3", data.contestantId, data.playerId, data.competitionId, data.tournamentCalendarId);

            var result = kendb.Fetch<TablePersonCareerStats>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonPerson_CareerStats table : " + DateTime.Now);
                    result = new TablePersonCareerStats();
                    result.id = data.id;
                    result.goals = data.goals;
                    result.assists = data.assists;
                    result.penaltyGoals = data.penaltyGoals;
                    result.appearances = data.appearances;
                    result.yellowCards = data.yellowCards;
                    result.secondYellowCards = data.secondYellowCards;
                    result.redCards = data.redCards;
                    result.substituteIn = data.substituteIn;
                    result.substituteOut = data.substituteOut;
                    result.subsOnBench = data.subsOnBench;
                    result.minutesPlayed = data.minutesPlayed;
                    result.shirtNumber = data.shirtNumber;
                    result.competitionId = data.competitionId;
                    result.tournamentCalendarId = data.tournamentCalendarId;
                    result.contestantId = data.contestantId;
                    result.playerId = data.playerId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2} : {3} : {4}", DateTime.Now, result.contestantId, result.playerId, result.competitionId, result.tournamentCalendarId));
                }
                else
                {
                    logDebug("Updating SDCommonPerson_CareerStats table : " + DateTime.Now);
                    //result.id = data.id;
                    result.goals = (data.goals == 0) ? result.goals : data.goals;
                    result.assists = (data.assists == 0) ? result.assists : data.assists;
                    result.penaltyGoals = (data.penaltyGoals == 0) ? result.penaltyGoals : data.penaltyGoals;
                    result.appearances = (data.appearances == 0) ? result.appearances : data.appearances;
                    result.yellowCards = (data.yellowCards == 0) ? result.yellowCards : data.yellowCards;
                    result.secondYellowCards = (data.secondYellowCards == 0) ? result.secondYellowCards : data.secondYellowCards;
                    result.redCards = (data.redCards == 0) ? result.redCards : data.redCards;
                    result.substituteIn = (data.substituteIn == 0) ? result.substituteIn : data.substituteIn;
                    result.substituteOut = (data.substituteOut == 0) ? result.substituteOut : data.substituteOut;
                    result.subsOnBench = (data.subsOnBench == 0) ? result.subsOnBench : data.subsOnBench;
                    result.minutesPlayed = (data.minutesPlayed == 0) ? result.minutesPlayed : data.minutesPlayed;
                    result.shirtNumber = (data.shirtNumber == 0) ? result.shirtNumber : data.shirtNumber;
                    result.competitionId = string.IsNullOrEmpty(data.competitionId) ? result.competitionId : data.competitionId;
                    result.tournamentCalendarId = string.IsNullOrEmpty(data.tournamentCalendarId) ? result.tournamentCalendarId : data.tournamentCalendarId;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.playerId = string.IsNullOrEmpty(data.playerId) ? result.playerId : data.playerId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2} : {3} : {4}", DateTime.Now, result.contestantId, result.playerId, result.competitionId, result.tournamentCalendarId));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public int InsertOrUpdateHeadToHeadSummary(TableHeadToHeadSummary data)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommonMatch_HeadToHeadSummary where matchId=@0", data.matchId);

            var result = kendb.Fetch<TableHeadToHeadSummary>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_HeadToHeadSummary table : " + DateTime.Now);
                    result = new TableHeadToHeadSummary();
                    result.id = data.id;
                    result.homeWin = data.homeWin;
                    result.awayWin = data.awayWin;
                    result.draw = data.draw;
                    result.homeScore = data.homeScore;
                    result.awayScore = data.awayScore;
                    result.matchId = data.matchId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.matchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_HeadToHeadSummary table : " + DateTime.Now);
                    //result.id = data.id;
                    result.homeWin = (data.homeWin == 0) ? result.homeWin : data.homeWin;
                    result.awayWin = (data.awayWin == 0) ? result.awayWin : data.awayWin;
                    result.draw = (data.draw == 0) ? result.draw : data.draw;
                    result.homeScore = (data.homeScore == 0) ? result.homeScore : data.homeScore;
                    result.awayScore = (data.awayScore == 0) ? result.awayScore : data.awayScore;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.matchId));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public int InsertOrUpdateHeadToHeadMatches(TableHeadToHeadMatches data)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommonMatch_HeadToHeadMatch where matchId=@0 and hthMatchId = @1", data.matchId, data.hthMatchId);

            var result = kendb.Fetch<TableHeadToHeadMatches>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_HeadToHeadMatch table : " + DateTime.Now);
                    result = new TableHeadToHeadMatches();
                    result.id = data.id;
                    result.matchId = data.matchId;
                    result.hthMatchId = data.hthMatchId;
                    result.teamAId = data.teamAId;
                    result.teamAName = data.teamAName;
                    result.teamBId = data.teamBId;
                    result.teamBName = data.teamBName;
                    result.homeScore = data.homeScore;
                    result.awayScore = data.awayScore;
                    result.matchDate = data.matchDate;
                    result.competitionId = data.competitionId;
                    result.tcalendarId = data.tcalendarId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2}", DateTime.Now, result.matchId, result.hthMatchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_HeadToHeadMatch table : " + DateTime.Now);
                    //result.id = data.id;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    result.hthMatchId = string.IsNullOrEmpty(data.hthMatchId) ? result.hthMatchId : data.hthMatchId;
                    result.teamAId = string.IsNullOrEmpty(data.teamAId) ? result.teamAId : data.teamAId;
                    result.teamAName = string.IsNullOrEmpty(data.teamAName) ? result.teamAName : data.teamAName;
                    result.teamBId = string.IsNullOrEmpty(data.teamBId) ? result.matchId : data.teamBId;
                    result.teamBName = string.IsNullOrEmpty(data.teamBName) ? result.teamBName : data.teamBName;
                    result.homeScore = (data.homeScore == 0) ? result.homeScore : data.homeScore;
                    result.awayScore = (data.awayScore == 0) ? result.awayScore : data.awayScore;
                    result.matchDate = (data.matchDate == null || data.matchDate == DateTime.MinValue) ? (result.matchDate != null ? result.matchDate : null) : data.matchDate;
                    result.competitionId = string.IsNullOrEmpty(data.competitionId) ? result.competitionId : data.competitionId;
                    result.tcalendarId = string.IsNullOrEmpty(data.tcalendarId) ? result.tcalendarId : data.tcalendarId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2}", DateTime.Now, result.matchId, result.hthMatchId));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public int InsertOrUpdateMatchForm(TableMatchForm data)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommonMatch_Form where matchId=@0 and hthMatchId = @1", data.matchId, data.hthMatchId);

            var result = kendb.Fetch<TableMatchForm>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDCommonMatch_Form table : " + DateTime.Now);
                    result = new TableMatchForm();
                    result.id = data.id;
                    result.matchId = data.matchId;
                    result.contestantId = data.contestantId;
                    result.hthMatchId = data.hthMatchId;
                    result.teamAId = data.teamAId;
                    result.teamAName = data.teamAName;
                    result.teamBId = data.teamBId;
                    result.teamBName = data.teamBName;
                    result.homeScore = data.homeScore;
                    result.awayScore = data.awayScore;
                    result.matchDate = data.matchDate;
                    result.competitionId = data.competitionId;
                    result.tcalendarId = data.tcalendarId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1} : {2}", DateTime.Now, result.matchId, result.hthMatchId));
                }
                else
                {
                    logDebug("Updating SDCommonMatch_Form table : " + DateTime.Now);
                    //result.id = data.id;
                    result.matchId = string.IsNullOrEmpty(data.matchId) ? result.matchId : data.matchId;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.hthMatchId = string.IsNullOrEmpty(data.hthMatchId) ? result.hthMatchId : data.hthMatchId;
                    result.teamAId = string.IsNullOrEmpty(data.teamAId) ? result.teamAId : data.teamAId;
                    result.teamAName = string.IsNullOrEmpty(data.teamAName) ? result.teamAName : data.teamAName;
                    result.teamBId = string.IsNullOrEmpty(data.teamBId) ? result.matchId : data.teamBId;
                    result.teamBName = string.IsNullOrEmpty(data.teamBName) ? result.teamBName : data.teamBName;
                    result.homeScore = (data.homeScore == 0) ? result.homeScore : data.homeScore;
                    result.awayScore = (data.awayScore == 0) ? result.awayScore : data.awayScore;
                    result.matchDate = (data.matchDate == null || data.matchDate == DateTime.MinValue) ? (result.matchDate != null ? result.matchDate : null) : data.matchDate;
                    result.competitionId = string.IsNullOrEmpty(data.competitionId) ? result.competitionId : data.competitionId;
                    result.tcalendarId = string.IsNullOrEmpty(data.tcalendarId) ? result.tcalendarId : data.tcalendarId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1} : {2}", DateTime.Now, result.matchId, result.hthMatchId));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertRanking(TableRanking data)
        {
            try
            {               
                logDebug("Inserting to SDCommon_Ranking table : " + DateTime.Now);
                var result = new TableRanking();
                //result.id = data.id;
                result.id = Guid.NewGuid().ToString();
                result.type = data.type;
                result.playerId = data.playerId;
                result.contestantId = data.contestantId;
                result.value = data.value;
                result.rank = data.rank;
                result.competitionId = data.competitionId;
                result.tcalendarId = data.tcalendarId;

                kendb.Insert(result);
                logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }
        #endregion

        #region Delete
        public bool DeleteRanking(string compId, string tcId)
        {
            try
            {
                logDebug("Deleting SDCommon_Ranking table : " + DateTime.Now);

                var sql = Sql.Builder;
                sql = Sql.Builder.Append(@"select count(id) as totRecord from SDCommon_Ranking where competitionId=@0 and tcalendarId = @1", compId, tcId);

                var totRecord = kendb.ExecuteScalar<int>(sql);

                sql = Sql.Builder.Append(@"delete SDCommon_Ranking where competitionId=@0 and tcalendarId=@1 ", compId, tcId);
                var result = kendb.Execute(sql);

                if (totRecord == result)
                {
                    logDebug(string.Format("Deleting success : {0} : {1} : {2}", DateTime.Now, compId, tcId));
                    return true;
                }
                else
                {
                    logDebug(string.Format("Deleting failed : {0} : {1} : {2}", DateTime.Now, compId, tcId));
                    return false;
                }
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public bool DeleteContestantSquad(string contestantId, string tcId)
        {
            try
            {
                logDebug("Deleting SDCommonContestant_Squad table : " + DateTime.Now);

                var sql = Sql.Builder;
                sql = Sql.Builder.Append(@"select count(id) as totRecord from SDCommonContestant_Squad where contestantId=@0 and tcalendarId=@1", contestantId, tcId);

                var totRecord = kendb.ExecuteScalar<int>(sql);

                sql = Sql.Builder.Append(@"delete SDCommonContestant_Squad where contestantId=@0 and tcalendarId=@1", contestantId, tcId);
                var result = kendb.Execute(sql);

                if (totRecord == result)
                {
                    logDebug(string.Format("Deleting success : {0} : {1} : {2}", DateTime.Now, contestantId, tcId));
                    return true;
                }
                else
                {
                    logDebug(string.Format("Deleting failed : {0} : {1} : {2}", DateTime.Now, contestantId, tcId));
                    return false;
                }
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public bool DeleteContestantSquad(string tcId)
        {
            try
            {
                logDebug("Deleting SDCommonContestant_Squad table : " + DateTime.Now);

                var sql = Sql.Builder;
                sql = Sql.Builder.Append(@"select count(id) as totRecord from SDCommonContestant_Squad where tcalendarId=@0", tcId);

                var totRecord = kendb.ExecuteScalar<int>(sql);

                sql = Sql.Builder.Append(@"delete SDCommonContestant_Squad where tcalendarId=@0", tcId);
                var result = kendb.Execute(sql);

                if (totRecord == result)
                {
                    logDebug(string.Format("Deleting success : {0} : {1}", DateTime.Now, tcId));
                    return true;
                }
                else
                {
                    logDebug(string.Format("Deleting failed : {0} : {1}", DateTime.Now, tcId));
                    return false;
                }
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }
        #endregion

        #region Insert/Update MAL
        public string InsertOrUpdateMALTeam(TableMALTeam data)
        {
            var sql = Sql.Builder.Append(@"select * from MALSoccer_Team where id = @0", data.id);
            var result = kendb.Fetch<TableMALTeam>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALSoccer_Team table : " + DateTime.Now);
                    result = new TableMALTeam();
                    result.id = data.id;
                    result.newId = data.newId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALSoccer_Team table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateMALVenue(TableMALVenue data)
        {
            var sql = Sql.Builder.Append(@"select * from MALSoccer_Venue where id = @0", data.id);
            var result = kendb.Fetch<TableMALVenue>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALSoccer_Venue table : " + DateTime.Now);
                    result = new TableMALVenue();
                    result.id = data.id;
                    result.newId = data.newId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALSoccer_Venue table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateMALMatch(TableMALMatch data)
        {
            var sql = Sql.Builder.Append(@"select * from MALSoccer_Match where id = @0", data.id);
            var result = kendb.Fetch<TableMALMatch>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALSoccer_Match table : " + DateTime.Now);
                    result = new TableMALMatch();
                    result.id = data.id;
                    result.newId = data.newId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALSoccer_Match table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateMALPlayer(TableMALPlayer data)
        {
            var sql = Sql.Builder.Append(@"select * from MALSoccer_Player where id = @0", data.id);
            var result = kendb.Fetch<TableMALPlayer>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALSoccer_Player table : " + DateTime.Now);
                    result = new TableMALPlayer();
                    result.id = data.id;
                    result.newId = data.newId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALSoccer_Player table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateMALRound(TableMALRound data)
        {
            var sql = Sql.Builder.Append(@"select * from MALSoccer_Round where id = @0 and tcId = @1", data.id, data.tcId);
            var result = kendb.Fetch<TableMALRound>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALSoccer_Round table : " + DateTime.Now);
                    result = new TableMALRound();
                    result.id = data.id;
                    result.newId = data.newId;
                    result.tcId = data.tcId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALSoccer_Round table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateMALGroup(TableMALGroup data)
        {
            var sql = Sql.Builder.Append(@"select * from MALSoccer_Group where id = @0 and tcId = @1", data.id, data.tcId);
            var result = kendb.Fetch<TableMALGroup>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALSoccer_Group table : " + DateTime.Now);
                    result = new TableMALGroup();
                    result.id = data.id;
                    result.newId = data.newId;
                    result.tcId = data.tcId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALSoccer_Group table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public bool UpdateMatchSeriesByStageId(string seriesId, string stageId, string teamId)
        {
            var sql = Sql.Builder.Append(@"update SDCommon_Match set seriesId = @0 where stageId = @1 and teamAId = @2", seriesId, stageId, teamId);
            try
            {
                var result = kendb.Execute(sql);
                return result > 0;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }
        #endregion

        #region Insert/Update MAL Hockey
        public string InsertOrUpdateMALHockeyTeam(TableMALHockeyTeam data)
        {
            var sql = Sql.Builder.Append(@"select * from MALHockey_Team where id = @0", data.id);
            var result = kendb.Fetch<TableMALHockeyTeam>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALHockey_Team table : " + DateTime.Now);
                    result = new TableMALHockeyTeam();
                    result.id = data.id;
                    result.newId = data.newId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALHockey_Team table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateMALHockeyVenue(TableMALHockeyVenue data)
        {
            var sql = Sql.Builder.Append(@"select * from MALHockey_Venue where id = @0", data.id);
            var result = kendb.Fetch<TableMALHockeyVenue>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALHockeyVenue table : " + DateTime.Now);
                    result = new TableMALHockeyVenue();
                    result.id = data.id;
                    result.newId = data.newId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALHockey_Venue table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateMALHockeyMatch(TableMALHockeyMatch data)
        {
            var sql = Sql.Builder.Append(@"select * from MALHockey_Match where id = @0", data.id);
            var result = kendb.Fetch<TableMALHockeyMatch>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALHockey_Match table : " + DateTime.Now);
                    result = new TableMALHockeyMatch();
                    result.id = data.id;
                    result.newId = data.newId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALHockey_Match table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateMALHockeyPlayer(TableMALHockeyPlayer data)
        {
            var sql = Sql.Builder.Append(@"select * from MALHockey_Player where id = @0", data.id);
            var result = kendb.Fetch<TableMALHockeyPlayer>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALHockey_Player table : " + DateTime.Now);
                    result = new TableMALHockeyPlayer();
                    result.id = data.id;
                    result.newId = data.newId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALHockey_Player table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateMALHockeyRound(TableMALHockeyRound data)
        {
            var sql = Sql.Builder.Append(@"select * from MALHockey_Round where id = @0 and tcId = @1", data.id, data.tcId);
            var result = kendb.Fetch<TableMALHockeyRound>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALHockey_Round table : " + DateTime.Now);
                    result = new TableMALHockeyRound();
                    result.id = data.id;
                    result.newId = data.newId;
                    result.tcId = data.tcId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALHockey_Round table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public string InsertOrUpdateMALHockeyGroup(TableMALHockeyGroup data)
        {
            var sql = Sql.Builder.Append(@"select * from MALHockey_Group where id = @0 and tcId = @1", data.id, data.tcId);
            var result = kendb.Fetch<TableMALHockeyGroup>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to MALHockey_Group table : " + DateTime.Now);
                    result = new TableMALHockeyGroup();
                    result.id = data.id;
                    result.newId = data.newId;
                    result.tcId = data.tcId;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.id));
                }
                else
                {
                    logDebug("Existing data in MALHockey_Group table : " + DateTime.Now);
                    //result.id = data.id;
                    //result.teamId = string.IsNullOrEmpty(data.teamId) ? result.teamId : data.teamId;
                    //kendb.Update(result);
                }

                return result.newId;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public int InsertOrUpdateHockeyPersonCareerStats(TableHockeyPersonCareerStats data)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDHockeyPerson_CareerStats where contestantId=@0 and playerId=@1 and competitionId=@2 and tournamentCalendarId=@3", data.contestantId, data.playerId, data.competitionId, data.tournamentCalendarId);

            var result = kendb.Fetch<TableHockeyPersonCareerStats>(sql).FirstOrDefault();

            try
            {
                if (result == null)
                {
                    logDebug("Inserting to SDHockeyPerson_CareerStats table : " + DateTime.Now);
                    result = new TableHockeyPersonCareerStats();
                    result.id = data.id;
                    result.competitionId = data.competitionId;
                    result.tournamentCalendarId = data.tournamentCalendarId;
                    result.contestantId = data.contestantId;
                    result.playerId = data.playerId;
                    result.gamesCareer = data.gamesCareer;
                    result.gamesTournament = data.gamesTournament;
                    result.gamesSeason = data.gamesSeason;
                    result.startingCareer = data.startingCareer;
                    result.startingTournament = data.startingTournament;
                    result.startingSeason = data.startingSeason;
                    result.goalsTotalCareer = data.goalsTotalCareer;
                    result.goalsTotalTournament = data.goalsTotalTournament;
                    result.goalsTotalSeason = data.goalsTotalSeason;
                    result.goalsFieldCareer = data.goalsFieldCareer;
                    result.goalsFieldTournament = data.goalsFieldTournament;
                    result.goalsFieldSeason = data.goalsFieldSeason;
                    result.goalsPenaltyStrokeCareer = data.goalsPenaltyStrokeCareer;
                    result.goalsPenaltyStrokeTournament = data.goalsPenaltyStrokeTournament;
                    result.goalsPenaltyStrokeSeason = data.goalsPenaltyStrokeSeason;
                    result.goalsPenaltyStrokeMissCareer = data.goalsPenaltyStrokeMissCareer;
                    result.goalsPenaltyStrokeMissTournament = data.goalsPenaltyStrokeMissTournament;
                    result.goalsPenaltyStrokeMissSeason = data.goalsPenaltyStrokeMissSeason;
                    result.goalsPenaltyCornerCareer = data.goalsPenaltyCornerCareer;
                    result.goalsPenaltyCornerTournament = data.goalsPenaltyCornerTournament;
                    result.goalsPenaltyCornerSeason = data.goalsPenaltyCornerSeason;
                    result.goalsPenaltyCornerMissCareer = data.goalsPenaltyCornerMissCareer;
                    result.goalsPenaltyCornerMissTournament = data.goalsPenaltyCornerMissTournament;
                    result.goalsPenaltyCornerMissSeason = data.goalsPenaltyCornerMissSeason;
                    result.shotsTotalCareer = data.shotsTotalCareer;
                    result.shotsTotalTournament = data.shotsTotalTournament;
                    result.shotsTotalSeason = data.shotsTotalSeason;
                    result.shotsOnTargetCareer = data.shotsOnTargetCareer;
                    result.shotsOnTargetTournament = data.shotsOnTargetTournament;
                    result.shotsOnTargetSeason = data.shotsOnTargetSeason;
                    result.shotsOffTargetCareer = data.shotsOffTargetCareer;
                    result.shotsOffTargetTournament = data.shotsOffTargetTournament;
                    result.foulsCareer = data.foulsCareer;
                    result.foulsTournament = data.foulsTournament;
                    result.foulsSeason = data.foulsSeason;
                    result.tacklesCareer = data.tacklesCareer;
                    result.tacklesTournament = data.tacklesTournament;
                    result.tacklesSeason = data.tacklesSeason;
                    result.goalDoublesCareer = data.goalDoublesCareer;
                    result.goalDoublesTournament = data.goalDoublesTournament;
                    result.goalDoublesSeason = data.goalDoublesSeason;
                    result.goalHatTricksCareer = data.goalHatTricksCareer;
                    result.goalHatTricksTournament = data.goalHatTricksTournament;
                    result.goalHatTricksSeason = data.goalHatTricksSeason;
                    result.greenCardsCareer = data.greenCardsCareer;
                    result.greenCardsTournament = data.greenCardsTournament;
                    result.greenCardsSeason = data.greenCardsSeason;
                    result.yellowCardsCareer = data.yellowCardsCareer;
                    result.yellowCardsTournament = data.yellowCardsTournament;
                    result.yellowCardsSeason = data.yellowCardsSeason;
                    result.redCardsCareer = data.redCardsCareer;
                    result.redCardsTournament = data.redCardsTournament;
                    result.redCardsSeason = data.redCardsSeason;
                    result.turnoversCareer = data.turnoversCareer;
                    result.turnoversTournament = data.turnoversTournament;
                    result.turnoversSeason = data.turnoversSeason;
                    result.mistrapCareer = data.mistrapCareer;
                    result.mistrapTournament = data.mistrapTournament;
                    result.mistrapSeason = data.mistrapSeason;
                    result.eliminationPassesCareer = data.eliminationPassesCareer;
                    result.eliminationPassesTournament = data.eliminationPassesTournament;
                    result.eliminationPassesSeason = data.eliminationPassesSeason;
                    result.penetrationsTotalCareer = data.penetrationsTotalCareer;
                    result.penetrationsTotalTournament = data.penetrationsTotalTournament;
                    result.penetrationsTotalSeason = data.penetrationsTotalSeason;
                    result.penetrationsLeftCareer = data.penetrationsLeftCareer;
                    result.penetrationsLeftTournament = data.penetrationsLeftTournament;
                    result.penetrationsLeftSeason = data.penetrationsLeftSeason;
                    result.penetrationsCenterCareer = data.penetrationsCenterCareer;
                    result.penetrationsCenterTournament = data.penetrationsCenterTournament;
                    result.penetrationsCenterSeason = data.penetrationsCenterSeason;
                    result.penetrationsRightCareer = data.penetrationsRightCareer;
                    result.penetrationsRightTournament = data.penetrationsRightTournament;
                    result.penetrationsRightSeason = data.penetrationsRightSeason;
                    result.dragFlickDirectSuccessCareer = data.dragFlickDirectSuccessCareer;
                    result.dragFlickDirectSuccessTournament = data.dragFlickDirectSuccessTournament;
                    result.dragFlickDirectSuccessSeason = data.dragFlickDirectSuccessSeason;
                    result.dragFlickDirectFailCareer = data.dragFlickDirectFailCareer;
                    result.dragFlickDirectFailTournament = data.dragFlickDirectFailTournament;
                    result.dragFlickDirectFailSeason = data.dragFlickDirectFailSeason;
                    result.dragFlickIndirectSuccessCareer = data.dragFlickIndirectSuccessCareer;
                    result.dragFlickIndirectSuccessTournament = data.dragFlickIndirectSuccessTournament;
                    result.dragFlickIndirectSuccessSeason = data.dragFlickIndirectSuccessSeason;
                    result.dragFlickIndirectFailCareer = data.dragFlickIndirectFailCareer;
                    result.dragFlickIndirectFailTournament = data.dragFlickIndirectFailTournament;
                    result.dragFlickIndirectFailSeason = data.dragFlickIndirectFailSeason;
                    result.internationalGamesCareer = data.internationalGamesCareer;
                    kendb.Insert(result);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, result.contestantId, result.playerId, result.competitionId, result.tournamentCalendarId));
                }
                else
                {
                    logDebug("Updating SDHockeyPerson_CareerStats table : " + DateTime.Now);
                    //result.id = data.id;
                    result.gamesCareer = (data.gamesCareer < 0) ? result.gamesCareer : data.gamesCareer;
                    result.gamesTournament = (data.gamesTournament < 0) ? result.gamesTournament : data.gamesTournament;
                    result.gamesSeason = (data.gamesSeason < 0) ? result.gamesSeason : data.gamesSeason;
                    result.startingCareer = (data.startingCareer < 0) ? result.startingCareer : data.startingCareer;
                    result.startingTournament = (data.startingTournament < 0) ? result.startingTournament : data.startingTournament;
                    result.startingSeason = (data.startingSeason < 0) ? result.startingSeason : data.startingSeason;
                    result.goalsTotalCareer = (data.goalsTotalCareer < 0) ? result.goalsTotalCareer : data.goalsTotalCareer;
                    result.goalsTotalTournament = (data.goalsTotalTournament < 0) ? result.goalsTotalTournament : data.goalsTotalTournament;
                    result.goalsTotalSeason = (data.goalsTotalSeason < 0) ? result.goalsTotalSeason : data.goalsTotalSeason;
                    result.goalsFieldCareer = (data.goalsFieldCareer < 0) ? result.goalsFieldCareer : data.goalsFieldCareer;
                    result.goalsFieldTournament = (data.goalsFieldTournament < 0) ? result.goalsFieldTournament : data.goalsFieldTournament;
                    result.goalsFieldSeason = (data.goalsFieldSeason < 0) ? result.goalsFieldSeason : data.goalsFieldSeason;
                    result.goalsPenaltyStrokeCareer = (data.goalsPenaltyStrokeCareer < 0) ? result.goalsPenaltyStrokeCareer : data.goalsPenaltyStrokeCareer;
                    result.goalsPenaltyStrokeTournament = (data.goalsPenaltyStrokeTournament < 0) ? result.goalsPenaltyStrokeTournament : data.goalsPenaltyStrokeTournament;
                    result.goalsPenaltyStrokeSeason = (data.goalsPenaltyStrokeSeason < 0) ? result.goalsPenaltyStrokeSeason : data.goalsPenaltyStrokeSeason;
                    result.goalsPenaltyStrokeMissCareer = (data.goalsPenaltyStrokeMissCareer < 0) ? result.goalsPenaltyStrokeMissCareer : data.goalsPenaltyStrokeMissCareer;
                    result.goalsPenaltyStrokeMissTournament = (data.goalsPenaltyStrokeMissTournament < 0) ? result.goalsPenaltyStrokeMissTournament : data.goalsPenaltyStrokeMissTournament;
                    result.goalsPenaltyStrokeMissSeason = (data.goalsPenaltyStrokeMissSeason < 0) ? result.goalsPenaltyStrokeMissSeason : data.goalsPenaltyStrokeMissSeason;
                    result.goalsPenaltyCornerCareer = (data.goalsPenaltyCornerCareer < 0) ? result.goalsPenaltyCornerCareer : data.goalsPenaltyCornerCareer;
                    result.goalsPenaltyCornerTournament = (data.goalsPenaltyCornerTournament < 0) ? result.goalsPenaltyCornerTournament : data.goalsPenaltyCornerTournament;
                    result.goalsPenaltyCornerSeason = (data.goalsPenaltyCornerSeason < 0) ? result.goalsPenaltyCornerSeason : data.goalsPenaltyCornerSeason;
                    result.goalsPenaltyCornerMissCareer = (data.goalsPenaltyCornerMissCareer < 0) ? result.goalsPenaltyCornerMissCareer : data.goalsPenaltyCornerMissCareer;
                    result.goalsPenaltyCornerMissTournament = (data.goalsPenaltyCornerMissTournament < 0) ? result.goalsPenaltyCornerMissTournament : data.goalsPenaltyCornerMissTournament;
                    result.goalsPenaltyCornerMissSeason = (data.goalsPenaltyCornerMissSeason < 0) ? result.goalsPenaltyCornerMissSeason : data.goalsPenaltyCornerMissSeason;
                    result.shotsTotalCareer = (data.shotsTotalCareer < 0) ? result.shotsTotalCareer : data.shotsTotalCareer;
                    result.shotsTotalTournament = (data.shotsTotalTournament < 0) ? result.shotsTotalTournament : data.shotsTotalTournament;
                    result.shotsTotalSeason = (data.shotsTotalSeason < 0) ? result.shotsTotalSeason : data.shotsTotalSeason;
                    result.shotsOnTargetCareer = (data.shotsOnTargetCareer < 0) ? result.shotsOnTargetCareer : data.shotsOnTargetCareer;
                    result.shotsOnTargetTournament = (data.shotsOnTargetTournament < 0) ? result.shotsOnTargetTournament : data.shotsOnTargetTournament;
                    result.shotsOnTargetSeason = (data.shotsOnTargetSeason < 0) ? result.shotsOnTargetSeason : data.shotsOnTargetSeason;
                    result.shotsOffTargetCareer = (data.shotsOffTargetCareer < 0) ? result.shotsOffTargetCareer : data.shotsOffTargetCareer;
                    result.shotsOffTargetTournament = (data.shotsOffTargetTournament < 0) ? result.shotsOffTargetTournament : data.shotsOffTargetTournament;
                    result.foulsCareer = (data.foulsCareer < 0) ? result.foulsCareer : data.foulsCareer;
                    result.foulsTournament = (data.foulsTournament < 0) ? result.foulsTournament : data.foulsTournament;
                    result.foulsSeason = (data.foulsSeason < 0) ? result.foulsSeason : data.foulsSeason;
                    result.tacklesCareer = (data.tacklesCareer < 0) ? result.tacklesCareer : data.tacklesCareer;
                    result.tacklesTournament = (data.tacklesTournament < 0) ? result.tacklesTournament : data.tacklesTournament;
                    result.tacklesSeason = (data.tacklesSeason < 0) ? result.tacklesSeason : data.tacklesSeason;
                    result.goalDoublesCareer = (data.goalDoublesCareer < 0) ? result.goalDoublesCareer : data.goalDoublesCareer;
                    result.goalDoublesTournament = (data.goalDoublesTournament < 0) ? result.goalDoublesTournament : data.goalDoublesTournament;
                    result.goalDoublesSeason = (data.goalDoublesSeason < 0) ? result.goalDoublesSeason : data.goalDoublesSeason;
                    result.goalHatTricksCareer = (data.goalHatTricksCareer < 0) ? result.goalHatTricksCareer : data.goalHatTricksCareer;
                    result.goalHatTricksTournament = (data.goalHatTricksTournament < 0) ? result.goalHatTricksTournament : data.goalHatTricksTournament;
                    result.goalHatTricksSeason = (data.goalHatTricksSeason < 0) ? result.goalHatTricksSeason : data.goalHatTricksSeason;
                    result.greenCardsCareer = (data.greenCardsCareer < 0) ? result.greenCardsCareer : data.greenCardsCareer;
                    result.greenCardsTournament = (data.greenCardsTournament < 0) ? result.greenCardsTournament : data.greenCardsTournament;
                    result.greenCardsSeason = (data.greenCardsSeason < 0) ? result.greenCardsSeason : data.greenCardsSeason;
                    result.yellowCardsCareer = (data.yellowCardsCareer < 0) ? result.yellowCardsCareer : data.yellowCardsCareer;
                    result.yellowCardsTournament = (data.yellowCardsTournament < 0) ? result.yellowCardsTournament : data.yellowCardsTournament;
                    result.yellowCardsSeason = (data.yellowCardsSeason < 0) ? result.yellowCardsSeason : data.yellowCardsSeason;
                    result.redCardsCareer = (data.redCardsCareer < 0) ? result.redCardsCareer : data.redCardsCareer;
                    result.redCardsTournament = (data.redCardsTournament < 0) ? result.redCardsTournament : data.redCardsTournament;
                    result.redCardsSeason = (data.redCardsSeason < 0) ? result.redCardsSeason : data.redCardsSeason;
                    result.turnoversCareer = (data.turnoversCareer < 0) ? result.turnoversCareer : data.turnoversCareer;
                    result.turnoversTournament = (data.turnoversTournament < 0) ? result.turnoversTournament : data.turnoversTournament;
                    result.turnoversSeason = (data.turnoversSeason < 0) ? result.turnoversSeason : data.turnoversSeason;
                    result.mistrapCareer = (data.mistrapCareer < 0) ? result.mistrapCareer : data.mistrapCareer;
                    result.mistrapTournament = (data.mistrapTournament < 0) ? result.mistrapTournament : data.mistrapTournament;
                    result.mistrapSeason = (data.mistrapSeason < 0) ? result.mistrapSeason : data.mistrapSeason;
                    result.eliminationPassesCareer = (data.eliminationPassesCareer < 0) ? result.eliminationPassesCareer : data.eliminationPassesCareer;
                    result.eliminationPassesTournament = (data.eliminationPassesTournament < 0) ? result.eliminationPassesTournament : data.eliminationPassesTournament;
                    result.eliminationPassesSeason = (data.eliminationPassesSeason < 0) ? result.eliminationPassesSeason : data.eliminationPassesSeason;
                    result.penetrationsTotalCareer = (data.penetrationsTotalCareer < 0) ? result.penetrationsTotalCareer : data.penetrationsTotalCareer;
                    result.penetrationsTotalTournament = (data.penetrationsTotalTournament < 0) ? result.penetrationsTotalTournament : data.penetrationsTotalTournament;
                    result.penetrationsTotalSeason = (data.penetrationsTotalSeason < 0) ? result.penetrationsTotalSeason : data.penetrationsTotalSeason;
                    result.penetrationsLeftCareer = (data.penetrationsLeftCareer < 0) ? result.penetrationsLeftCareer : data.penetrationsLeftCareer;
                    result.penetrationsLeftTournament = (data.penetrationsLeftTournament < 0) ? result.penetrationsLeftTournament : data.penetrationsLeftTournament;
                    result.penetrationsLeftSeason = (data.penetrationsLeftSeason < 0) ? result.penetrationsLeftSeason : data.penetrationsLeftSeason;
                    result.penetrationsCenterCareer = (data.penetrationsCenterCareer < 0) ? result.penetrationsCenterCareer : data.penetrationsCenterCareer;
                    result.penetrationsCenterTournament = (data.penetrationsCenterTournament < 0) ? result.penetrationsCenterTournament : data.penetrationsCenterTournament;
                    result.penetrationsCenterSeason = (data.penetrationsCenterSeason < 0) ? result.penetrationsCenterSeason : data.penetrationsCenterSeason;
                    result.penetrationsRightCareer = (data.penetrationsRightCareer < 0) ? result.penetrationsRightCareer : data.penetrationsRightCareer;
                    result.penetrationsRightTournament = (data.penetrationsRightTournament < 0) ? result.penetrationsRightTournament : data.penetrationsRightTournament;
                    result.penetrationsRightSeason = (data.penetrationsRightSeason < 0) ? result.penetrationsRightSeason : data.penetrationsRightSeason;
                    result.dragFlickDirectSuccessCareer = (data.dragFlickDirectSuccessCareer < 0) ? result.dragFlickDirectSuccessCareer : data.dragFlickDirectSuccessCareer;
                    result.dragFlickDirectSuccessTournament = (data.dragFlickDirectSuccessTournament < 0) ? result.dragFlickDirectSuccessTournament : data.dragFlickDirectSuccessTournament;
                    result.dragFlickDirectSuccessSeason = (data.dragFlickDirectSuccessSeason < 0) ? result.dragFlickDirectSuccessSeason : data.dragFlickDirectSuccessSeason;
                    result.dragFlickDirectFailCareer = (data.dragFlickDirectFailCareer < 0) ? result.dragFlickDirectFailCareer : data.dragFlickDirectFailCareer;
                    result.dragFlickDirectFailTournament = (data.dragFlickDirectFailTournament < 0) ? result.dragFlickDirectFailTournament : data.dragFlickDirectFailTournament;
                    result.dragFlickDirectFailSeason = (data.dragFlickDirectFailSeason < 0) ? result.dragFlickDirectFailSeason : data.dragFlickDirectFailSeason;
                    result.dragFlickIndirectSuccessCareer = (data.dragFlickIndirectSuccessCareer < 0) ? result.dragFlickIndirectSuccessCareer : data.dragFlickIndirectSuccessCareer;
                    result.dragFlickIndirectSuccessTournament = (data.dragFlickIndirectSuccessTournament < 0) ? result.dragFlickIndirectSuccessTournament : data.dragFlickIndirectSuccessTournament;
                    result.dragFlickIndirectSuccessSeason = (data.dragFlickIndirectSuccessSeason < 0) ? result.dragFlickIndirectSuccessSeason : data.dragFlickIndirectSuccessSeason;
                    result.dragFlickIndirectFailCareer = (data.dragFlickIndirectFailCareer < 0) ? result.dragFlickIndirectFailCareer : data.dragFlickIndirectFailCareer;
                    result.dragFlickIndirectFailTournament = (data.dragFlickIndirectFailTournament < 0) ? result.dragFlickIndirectFailTournament : data.dragFlickIndirectFailTournament;
                    result.dragFlickIndirectFailSeason = (data.dragFlickIndirectFailSeason < 0) ? result.dragFlickIndirectFailSeason : data.dragFlickIndirectFailSeason;
                    result.internationalGamesCareer = (data.internationalGamesCareer < 0) ? result.internationalGamesCareer : data.internationalGamesCareer;
                    result.competitionId = string.IsNullOrEmpty(data.competitionId) ? result.competitionId : data.competitionId;
                    result.tournamentCalendarId = string.IsNullOrEmpty(data.tournamentCalendarId) ? result.tournamentCalendarId : data.tournamentCalendarId;
                    result.contestantId = string.IsNullOrEmpty(data.contestantId) ? result.contestantId : data.contestantId;
                    result.playerId = string.IsNullOrEmpty(data.playerId) ? result.playerId : data.playerId;
                    kendb.Update(result);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, result.contestantId, result.playerId, result.competitionId, result.tournamentCalendarId));
                }

                return result.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public int InsertOrUpdateHockeyTeamStats(TableHockeyTeamStats data)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDHockeyTeam_Stats where contestantId=@0 and hockeyTeamId=@1 and competitionId=@2 and calendarId=@3", data.contestantId, data.hockeyTeamId, data.competitionId, data.calendarId);

            var results = kendb.Fetch<TableHockeyTeamStats>(sql).FirstOrDefault();

            try
            {
                if (results == null)
                {
                    logDebug("Inserting to SDHockeyTeam_Stats table : " + DateTime.Now);
                    results = new TableHockeyTeamStats();
                    results.hockeyTeamId = data.hockeyTeamId;
                    results.contestantId = data.contestantId;
                    results.competitionId = data.competitionId;
                    results.calendarId = data.calendarId;
                    results.Matches_total = data.Matches_total;
                    results.Matches_total_home = data.Matches_total_home;
                    results.Matches_total_away = data.Matches_total_away;
                    results.Matches_total_played = data.Matches_total_played;
                    results.Matches_played_home = data.Matches_played_home;
                    results.Matches_played_away = data.Matches_played_away;
                    results.Matches_won_total = data.Matches_won_total;
                    results.Matches_won_home = data.Matches_won_home;
                    results.Matches_won_away = data.Matches_won_away;
                    results.Matches_draw_total = data.Matches_draw_total;
                    results.Matches_draw_home = data.Matches_draw_home;
                    results.Matches_draw_away = data.Matches_draw_away;
                    results.Matches_lost_total = data.Matches_lost_total;
                    results.Matches_lost_home = data.Matches_lost_home;
                    results.Matches_lost_away = data.Matches_lost_away;
                    results.Highest_win_home = data.Highest_win_home;
                    results.Highest_win_away = data.Highest_win_away;
                    results.Highest_lost_home = data.Highest_lost_home;
                    results.Highest_lost_away = data.Highest_lost_away;
                    results.Total_goals_for_and_against = data.Total_goals_for_and_against;
                    results.Total_goals_for_and_against_home = data.Total_goals_for_and_against_home;
                    results.Total_goals_for_and_against_away = data.Total_goals_for_and_against_away;
                    results.Total_goals_per_match = data.Total_goals_per_match;
                    results.Total_goals_per_match_home = data.Total_goals_per_match_home;
                    results.Total_goals_per_match_away = data.Total_goals_per_match_away;
                    results.Goals_for_total = data.Goals_for_total;
                    results.Goals_for_total_home = data.Goals_for_total_home;
                    results.Goals_for_total_away = data.Goals_for_total_away;
                    results.Goals_for_per_match_total = data.Goals_for_per_match_total;
                    results.Goals_for_per_match_home = data.Goals_for_per_match_home;
                    results.Goals_for_per_match_away = data.Goals_for_per_match_away;
                    results.Goals_against_total = data.Goals_against_total;
                    results.Goals_against_total_home = data.Goals_against_total_home;
                    results.Goals_against_total_away = data.Goals_against_total_away;
                    results.Goals_against_per_match_total = data.Goals_against_per_match_total;
                    results.Goals_against_per_match_home = data.Goals_against_per_match_home;
                    results.Goals_against_per_match_away = data.Goals_against_per_match_away;
                    results.No_goals_against_total = data.No_goals_against_total;
                    results.No_goals_against_home = data.No_goals_against_home;
                    results.No_goals_against_away = data.No_goals_against_away;
                    kendb.Insert(results);
                    logDebug(string.Format("Inserting success : {0} : {1}", DateTime.Now, results.contestantId, results.hockeyTeamId, results.competitionId, results.calendarId));
                }
                else
                {
                    logDebug("Updating SDHockeyTeam_Stats table : " + DateTime.Now);

                    results.Matches_total = (data.Matches_total < 0) ? results.Matches_total : data.Matches_total;
                    results.Matches_total_home = (data.Matches_total_home < 0) ? results.Matches_total_home : data.Matches_total_home;
                    results.Matches_total_away = (data.Matches_total_away < 0) ? results.Matches_total_away : data.Matches_total_away;
                    results.Matches_total_played = (data.Matches_total_played < 0) ? results.Matches_total_played : data.Matches_total_played;
                    results.Matches_played_home = (data.Matches_played_home < 0) ? results.Matches_played_home : data.Matches_played_home;
                    results.Matches_played_away = (data.Matches_played_away < 0) ? results.Matches_played_away : data.Matches_played_away;
                    results.Matches_won_total = (data.Matches_won_total < 0) ? results.Matches_won_total : data.Matches_won_total;
                    results.Matches_won_home = (data.Matches_won_home < 0) ? results.Matches_won_home : data.Matches_won_home;
                    results.Matches_won_away = (data.Matches_won_away < 0) ? results.Matches_won_away : data.Matches_won_away;
                    results.Matches_draw_total = (data.Matches_draw_total < 0) ? results.Matches_draw_total : data.Matches_draw_total;
                    results.Matches_draw_home = (data.Matches_draw_home < 0) ? results.Matches_draw_home : data.Matches_draw_home;
                    results.Matches_draw_away = (data.Matches_draw_away < 0) ? results.Matches_draw_away : data.Matches_draw_away;
                    results.Matches_lost_total = (data.Matches_lost_total < 0) ? results.Matches_lost_total : data.Matches_lost_total;
                    results.Matches_lost_home = (data.Matches_lost_home < 0) ? results.Matches_lost_home : data.Matches_lost_home;
                    results.Matches_lost_away = (data.Matches_lost_away < 0) ? results.Matches_lost_away : data.Matches_lost_away;
                    results.Highest_win_home = (data.Highest_win_home < 0) ? results.Highest_win_home : data.Highest_win_home;
                    results.Highest_win_away = (data.Highest_win_away < 0) ? results.Highest_win_away : data.Highest_win_away;
                    results.Highest_lost_home = (data.Highest_lost_home < 0) ? results.Highest_lost_home : data.Highest_lost_home;
                    results.Highest_lost_away = (data.Highest_lost_away < 0) ? results.Highest_lost_away : data.Highest_lost_away;
                    results.Total_goals_for_and_against = (data.Total_goals_for_and_against < 0) ? results.Total_goals_for_and_against : data.Total_goals_for_and_against;
                    results.Total_goals_for_and_against_home = (data.Total_goals_for_and_against_home < 0) ? results.Total_goals_for_and_against_home : data.Total_goals_for_and_against_home;
                    results.Total_goals_for_and_against_away = (data.Total_goals_for_and_against_away < 0) ? results.Total_goals_for_and_against_away : data.Total_goals_for_and_against_away;
                    results.Total_goals_per_match = (data.Total_goals_per_match < 0) ? results.Total_goals_per_match : data.Total_goals_per_match;
                    results.Total_goals_per_match_home = (data.Total_goals_per_match_home < 0) ? results.Total_goals_per_match_home : data.Total_goals_per_match_home;
                    results.Total_goals_per_match_away = (data.Total_goals_per_match_away < 0) ? results.Total_goals_per_match_away : data.Total_goals_per_match_away;
                    results.Goals_for_total = (data.Goals_for_total < 0) ? results.Goals_for_total : data.Goals_for_total;
                    results.Goals_for_total_home = (data.Goals_for_total_home < 0) ? results.Goals_for_total_home : data.Goals_for_total_home;
                    results.Goals_for_total_away = (data.Goals_for_total_away < 0) ? results.Goals_for_total_away : data.Goals_for_total_away;
                    results.Goals_for_per_match_total = (data.Goals_for_per_match_total < 0) ? results.Goals_for_per_match_total : data.Goals_for_per_match_total;
                    results.Goals_for_per_match_home = (data.Goals_for_per_match_home < 0) ? results.Goals_for_per_match_home : data.Goals_for_per_match_home;
                    results.Goals_for_per_match_away = (data.Goals_for_per_match_away < 0) ? results.Goals_for_per_match_away : data.Goals_for_per_match_away;
                    results.Goals_against_total = (data.Goals_against_total < 0) ? results.Goals_against_total : data.Goals_against_total;
                    results.Goals_against_total_home = (data.Goals_against_total_home < 0) ? results.Goals_against_total_home : data.Goals_against_total_home;
                    results.Goals_against_total_away = (data.Goals_against_total_away < 0) ? results.Goals_against_total_away : data.Goals_against_total_away;
                    results.Goals_against_per_match_total = (data.Goals_against_per_match_total < 0) ? results.Goals_against_per_match_total : data.Goals_against_per_match_total;
                    results.Goals_against_per_match_home = (data.Goals_against_per_match_home < 0) ? results.Goals_against_per_match_home : data.Goals_against_per_match_home;
                    results.Goals_against_per_match_away = (data.Goals_against_per_match_away < 0) ? results.Goals_against_per_match_away : data.Goals_against_per_match_away;
                    results.No_goals_against_total = (data.No_goals_against_total < 0) ? results.No_goals_against_total : data.No_goals_against_total;
                    results.No_goals_against_home = (data.No_goals_against_home < 0) ? results.No_goals_against_home : data.No_goals_against_home;
                    results.No_goals_against_away = (data.No_goals_against_away < 0) ? results.No_goals_against_away : data.No_goals_against_away;
                    kendb.Update(results);
                    logDebug(string.Format("Updating success : {0} : {1}", DateTime.Now, results.contestantId, results.hockeyTeamId, results.competitionId, results.calendarId));
                }

                return results.id;
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }
        #endregion

        #region Select MAL
        public TableTournamentCalendar getTournamentCalendar(string compId)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommon_TournamentCalendar where competitionId=@0 and active='yes'", compId);
            var result = kendb.Fetch<TableTournamentCalendar>(sql).FirstOrDefault();
            return result;
        }

        public TableTournamentCalendar getTournamentCalendarByTcid(string compId, string tcId)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommon_TournamentCalendar where competitionId=@0 and id=@1 and active='yes'", compId, tcId);
            var result = kendb.Fetch<TableTournamentCalendar>(sql).FirstOrDefault();
            return result;
        }

        public TableCompetition getCompetition(string compId)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommon_Competition where id=@0", compId);
            var result = kendb.Fetch<TableCompetition>(sql).FirstOrDefault();
            return result;
        }

        public TableMatchInfo getMatch(string matchId)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommon_Match where id=@0", matchId);
            var result = kendb.Fetch<TableMatchInfo>(sql).FirstOrDefault();
            return result;
        }

        public List<TableMatchInfo> getMatchesByStatus(string tcId, string status)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommon_Match where tcalendarId=@0 and matchStatus=@1", tcId, status);
            var result = kendb.Fetch<TableMatchInfo>(sql);
            return result;
        }

        public TablePerson getPerson(string playerId)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommon_Person where id=@0", playerId);
            var result = kendb.Fetch<TablePerson>(sql).FirstOrDefault();
            return result;
        }

        public List<TableStage> getStage(string tcId)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select s.id, s.startDate, s.endDate, s.name from SDCommon_Stage s inner join MALSoccer_Round r on s.id = r. newId where r.tcId=@0", tcId);
            var result = kendb.Fetch<TableStage>(sql);
            return result;
        }

        public List<TableStage> getHockeyStage(string tcId)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select s.id, s.startDate, s.endDate, s.name from SDCommon_Stage s inner join MALHockey_Round r on s.id = r. newId where r.tcId=@0", tcId);
            var result = kendb.Fetch<TableStage>(sql);
            return result;
        }

        public TableSport getSport(string sportType)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommon_Sport where name=@0", sportType);
            var result = kendb.Fetch<TableSport>(sql).FirstOrDefault();
            return result;
        }

        public TableRuleset getRuleset(string ruleSet)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from SDCommon_RuleSet where name=@0", ruleSet);
            var result = kendb.Fetch<TableRuleset>(sql).FirstOrDefault();
            return result;
        }

        public List<TableMALTeam> getAllMALTeam()
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from MALSoccer_Team");
            var result = kendb.Fetch<TableMALTeam>(sql);
            return result;
        }

        public List<TableMALHockeyTeam> getAllMALHockeyTeam()
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from MALHockey_Team");
            var result = kendb.Fetch<TableMALHockeyTeam>(sql);
            return result;
        }

        public List<TableMALMatch> getAllMALMatchByTournamentCalendar(string tcId)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select mm.id, mm.newId from MALSoccer_Match mm inner join SDCommon_Match m on mm.newId = m.id and m.tcalendarId = @0", tcId);
            var result = kendb.Fetch<TableMALMatch>(sql);
            return result;
        }

        public List<TableMALPlayer> getAllMALPlayer()
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from MALSoccer_Player");
            var result = kendb.Fetch<TableMALPlayer>(sql);
            return result;
        }

        public List<TableMALHockeyMatch> getAllMALHockeyMatchByTournamentCalendar(string tcId)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select mm.id, mm.newId from MALHockey_Match mm inner join SDCommon_Match m on mm.newId = m.id and m.tcalendarId = @0", tcId);
            var result = kendb.Fetch<TableMALHockeyMatch>(sql);
            return result;
        }

        public List<TableMALRound> getAllMALRound(string tcId)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from MALSoccer_Round where tcId = @0", tcId);
            var result = kendb.Fetch<TableMALRound>(sql);
            return result;
        }

        public List<TableMALHockeyRound> getAllMALHockeyRound(string tcId)
        {
            var sql = Sql.Builder;
            sql = Sql.Builder.Append(@"select * from MALHockey_Round where tcId = @0", tcId);
            var result = kendb.Fetch<TableMALHockeyRound>(sql);
            return result;
        }
        #endregion

        #region Delete MAL
        public bool DeleteMatchLineup(string matchId, string playerId, string contestantId)
        {
            try
            {
                logDebug("Deleting SDCommonMatch_Lineups table : " + DateTime.Now);

                var sql = Sql.Builder.Append(@"delete SDCommonMatch_Lineups where playerId=@0 and contestantId=@1 and matchId=@2", playerId, contestantId, matchId);
                var result = kendb.Execute(sql);

                if (result == 1)
                {
                    logDebug(string.Format("Deleting success : {0} : {1} : {2} : {3}", DateTime.Now, playerId, contestantId, matchId));
                    return true;
                }
                else
                {
                    logDebug(string.Format("Deleting failed : {0} : {1} : {2} : {3}", DateTime.Now, playerId, contestantId, matchId));
                    return false;
                }
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public bool DeleteMatchSubstitution(string matchId, string playerOnId, string playerOffId, string contestantId)
        {
            try
            {
                logDebug("Deleting SDCommonMatch_Substitutions table : " + DateTime.Now);

                var sql = Sql.Builder.Append(@"delete SDCommonMatch_Substitutions where playerOnId=@0 and playerOffId=@1 and contestantId=@2 and matchId=@3", playerOnId, playerOffId, contestantId, matchId);
                var result = kendb.Execute(sql);

                if (result == 1)
                {
                    logDebug(string.Format("Deleting success : {0} : {1} : {2} : {3} : {4}", DateTime.Now, playerOnId, playerOffId, contestantId, matchId));
                    return true;
                }
                else
                {
                    logDebug(string.Format("Deleting failed : {0} : {1} : {2} : {3} : {4}", DateTime.Now, playerOnId, playerOffId, contestantId, matchId));
                    return false;
                }
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public bool DeleteMatchGoals(string matchId, string playerId, string type, int timeMin, int timeMinPlus)
        {
            try
            {
                logDebug("Deleting SDCommonMatch_Goals table : " + DateTime.Now);

                var sql = Sql.Builder.Append(@"delete SDCommonMatch_Goals where playerId=@0 and type=@1 and timeMin=@2 and timeMinPlus=@3 and matchId=@4", playerId, type, timeMin, timeMinPlus, matchId);
                var result = kendb.Execute(sql);

                if (result == 1)
                {
                    logDebug(string.Format("Deleting success : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, playerId, type, timeMin, timeMinPlus, matchId));
                    return true;
                }
                else
                {
                    logDebug(string.Format("Deleting failed : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, playerId, type, timeMin, timeMinPlus, matchId));
                    return false;
                }
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public bool DeleteMatchAssists(string matchId, string playerId, string type, int timeMin, int timeMinPlus)
        {
            try
            {
                logDebug("Deleting SDCommonMatch_Assists table : " + DateTime.Now);

                var sql = Sql.Builder.Append(@"delete SDCommonMatch_Assists where playerId=@0 and type=@1 and timeMin=@2 and timeMinPlus=@3 and matchId=@4", playerId, type, timeMin, timeMinPlus, matchId);
                var result = kendb.Execute(sql);

                if (result == 1)
                {
                    logDebug(string.Format("Deleting success : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, playerId, type, timeMin, timeMinPlus, matchId));
                    return true;
                }
                else
                {
                    logDebug(string.Format("Deleting failed : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, playerId, type, timeMin, timeMinPlus, matchId));
                    return false;
                }
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public bool DeleteMatchBookings(string matchId, string playerId, string type, int timeMin, int timeMinPlus)
        {
            try
            {
                logDebug("Deleting SDCommonMatch_Bookings table : " + DateTime.Now);

                var sql = Sql.Builder.Append(@"delete SDCommonMatch_Bookings where playerId=@0 and type=@1 and timeMin=@2 and timeMinPlus=@3 and matchId=@4", playerId, type, timeMin, timeMinPlus, matchId);
                var result = kendb.Execute(sql);

                if (result == 1)
                {
                    logDebug(string.Format("Deleting success : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, playerId, type, timeMin, timeMinPlus, matchId));
                    return true;
                }
                else
                {
                    logDebug(string.Format("Deleting failed : {0} : {1} : {2} : {3} : {4} : {5}", DateTime.Now, playerId, type, timeMin, timeMinPlus, matchId));
                    return false;
                }
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }

        public bool DeleteMatchSubstitutions(string matchId, string contestantId, string playerOnId, string playerOffId)
        {
            try
            {
                logDebug("Deleting SDCommonMatch_Substitutions table : " + DateTime.Now);

                var sql = Sql.Builder.Append(@"select * from SDCommonMatch_Substitutions where playerOnId=@0 and playerOffId=@1 and contestantId=@2 and matchId=@3", playerOnId, playerOffId, contestantId, matchId);
                var result = kendb.Execute(sql);

                if (result == 1)
                {
                    logDebug(string.Format("Deleting success : {0} : {1} : {2} : {3} : {4}", DateTime.Now, playerOnId, playerOffId, contestantId, matchId));
                    return true;
                }
                else
                {
                    logDebug(string.Format("Deleting failed : {0} : {1} : {2} : {3} : {4}", DateTime.Now, playerOnId, playerOffId, contestantId, matchId));
                    return false;
                }
            }
            catch (Exception ex)
            {
                logError(ex.ToString());
                throw ex;
            }
        }
        #endregion

        #region Helper
        public string RandomString(int length)
        {
            string input = "abcdefghijklmnopqrstuvwxyz0123456789";
            var chars = Enumerable.Range(0, length)
                                   .Select(x => input[random.Next(0, input.Length)]);
            return new string(chars.ToArray()).ToLower();
        }
        #endregion
    }

    #region Tables
    [PetaPoco.TableName("SDCommon_TournamentCalendar")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableTournamentCalendar
    {
        public string id { get; set; }
        public int ocId { get; set; }
        public string name { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string active { get; set; }
        public DateTime? lastUpdated { get; set; }
        public string competitionId { get; set; }
    }

    [PetaPoco.TableName("SDCommon_Competition")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableCompetition
    {
        public string id { get; set; }
        public int ocId { get; set; }
        public int opId { get; set; }
        public string name { get; set; }
        public string countryId { get; set; }
    }

    [PetaPoco.TableName("SDCommon_Sport")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableSport
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    [PetaPoco.TableName("SDCommon_RuleSet")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableRuleset
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    [PetaPoco.TableName("SDCommon_Country")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableCountry
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    [PetaPoco.TableName("SDCommon_Stage")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableStage
    {
        public string id { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string name { get; set; }
    }

    [PetaPoco.TableName("SDCommon_Series")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableSeries
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    [PetaPoco.TableName("SDCommon_Contestant")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableContestant
    {
        public string id { get; set; }
        public string teamType { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public string clubName { get; set; }
        public string code { get; set; }
        public string countryId { get; set; }
    }

    [PetaPoco.TableName("SDCommon_Venue")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableVenue
    {
        public string id { get; set; }
        public string name { get; set; }
        public string longName { get; set; }
    }

    [PetaPoco.TableName("SDCommon_Match")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableMatchInfo
    {
        public string id { get; set; }
        public string sportId { get; set; }
        public string ruleSetId { get; set; }
        public string description { get; set; }
        public string teamAId { get; set; }
        public string teamBId { get; set; }
        public DateTime? date { get; set; }
        public string week { get; set; }
        public string competitionId { get; set; }
        public string tcalendarId { get; set; }
        public string stageId { get; set; }
        public string seriesId { get; set; }
        public string venueId { get; set; }
        public DateTime? lastUpdated { get; set; }
        public string matchStatus { get; set; }
        public string winner { get; set; }
        public long matchLengthMin { get; set; }
        public long matchLengthSec { get; set; }
        public int scoreHtHome { get; set; }
        public int scoreHtAway { get; set; }
        public int scoreFtHome { get; set; }
        public int scoreFtAway { get; set; }
        public int scoreEtHome { get; set; }
        public int scoreEtAway { get; set; }
        public int scorePenHome { get; set; }
        public int scorePenAway { get; set; }
        public int scoreTotHome { get; set; }
        public int scoreTotAway { get; set; }
        public int scoreAggHome { get; set; }
        public int scoreAggAway { get; set; }
        public long matchPAId { get; set; }
        public long videoId { get; set; }
        public long attendance { get; set; }
        public string weather { get; set; }
        public long periodId { get; set; }
        public long periodCount { get; set; }
        public string aggregateWinnerId { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_Goals")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableMatchGoals
    {
        public int id { get; set; }
        public long optaEventId { get; set; }
        public string type { get; set; }
        public string playerId { get; set; }
        public string contestantId { get; set; }
        public int timeMin { get; set; }
        public int timeMinPlus { get; set; }
        public int periodId { get; set; }
        public DateTime? lastUpdated { get; set; }
        public string matchId { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_Bookings")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableMatchBookings
    {
        public int id { get; set; }
        public long optaEventId { get; set; }
        public string type { get; set; }
        public string playerId { get; set; }
        public string contestantId { get; set; }
        public int timeMin { get; set; }
        public int timeMinPlus { get; set; }
        public int periodId { get; set; }
        public DateTime? lastUpdated { get; set; }
        public string matchId { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_Assists")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableMatchAssists
    {
        public int id { get; set; }
        public long optaEventId { get; set; }
        public string type { get; set; }
        public string playerId { get; set; }
        public string contestantId { get; set; }
        public int timeMin { get; set; }
        public int timeMinPlus { get; set; }
        public int periodId { get; set; }
        public DateTime? lastUpdated { get; set; }
        public string matchId { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_PenaltyShot")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableMatchPenaltyShot
    {
        public int id { get; set; }
        public long optaEventId { get; set; }
        public string playerId { get; set; }
        public string contestantId { get; set; }
        public string outcome { get; set; }
        public int teamPenaltyNumber { get; set; }
        public DateTime? lastUpdated { get; set; }
        public string matchId { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_Lineups")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableMatchLineups
    {
        public int id { get; set; }
        public long optaEventId { get; set; }
        public string playerId { get; set; }
        public int shirtNumber { get; set; }
        public string position { get; set; }
        public string positionSide { get; set; }
        public string contestantId { get; set; }
        public string matchId { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_Substitutions")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableMatchSubstitutions
    {
        public int id { get; set; }
        public long optaEventId { get; set; }
        public string playerOnId { get; set; }
        public string playerOffId { get; set; }
        public string contestantId { get; set; }
        public int timeMin { get; set; }
        public int timeMinPlus { get; set; }
        public int periodId { get; set; }
        public DateTime? lastUpdated { get; set; }
        public string matchId { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_Officials")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableMatchOfficials
    {
        public int id { get; set; }
        public long optaEventId { get; set; }
        public string officialId { get; set; }
        public string type { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string matchId { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_TeamStats")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableMatchTeamStats
    {
        public int id { get; set; }
        public long optaEventId { get; set; }
        public double fh { get; set; }
        public double sh { get; set; }
        public string type { get; set; }
        public double value { get; set; }
        public string contestantId { get; set; }
        public string matchId { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_PlayerStats")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableMatchPlayerStats
    {
        public int id { get; set; }
        public long optaEventId { get; set; }
        public string type { get; set; }
        public int value { get; set; }
        public string playerId { get; set; }
        public string matchId { get; set; }
    }

    [PetaPoco.TableName("SDCommon_Standing")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableStanding
    {
        public int id { get; set; }
        public int rank { get; set; }
        public int lastRank { get; set; }
        public string contestantId { get; set; }
        public int points { get; set; }
        public int matchesPlayed { get; set; }
        public int matchesWon { get; set; }
        public int matchesLost { get; set; }
        public int matchesDrawn { get; set; }
        public int goalsFor { get; set; }
        public int goalsAgainst { get; set; }
        public string goaldifference { get; set; }
        public string type { get; set; }
        public string seriesId { get; set; }
        public string stageId { get; set; }
        public string tcalendarId { get; set; }
        public string competitionId { get; set; }
    }

    [PetaPoco.TableName("SDCommon_Person")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TablePerson
    {
        public string id { get; set; }
        public string type { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string matchName { get; set; }
        public DateTime? dateOfBirth { get; set; }
        public string placeOfBirth { get; set; }
        public string countryOfBirthId { get; set; }
        public string countryOfBirth { get; set; }
        public int height { get; set; }
        public int weight { get; set; }
        public string foot { get; set; }
        public string status { get; set; }
        public string nationalityId { get; set; }
        public string position { get; set; }
        public int shirtNumber { get; set; }
        public DateTime? lastUpdated { get; set; }
    }

    [PetaPoco.TableName("SDCommonContestant_Squad")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableContestantSquad
    {
        public int id { get; set; }
        public string contestantId { get; set; }
        public string playerId { get; set; }
        public string tcalendarId { get; set; }
    }

    [PetaPoco.TableName("SDCommonPerson_Career")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TablePersonCareer
    {
        public int id { get; set; }
        public string contestantId { get; set; }
        public string contestantType { get; set; }
        public string active { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string role { get; set; }
        public string playerId { get; set; }
    }

    [PetaPoco.TableName("SDCommonPerson_CareerStats")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TablePersonCareerStats
    {
        public int id { get; set; }
        public int goals { get; set; }
        public int assists { get; set; }
        public int penaltyGoals { get; set; }
        public int appearances { get; set; }
        public int yellowCards { get; set; }
        public int secondYellowCards { get; set; }
        public int redCards { get; set; }
        public int substituteIn { get; set; }
        public int substituteOut { get; set; }
        public int subsOnBench { get; set; }
        public int minutesPlayed { get; set; }
        public int shirtNumber { get; set; }
        public string competitionId { get; set; }
        public string tournamentCalendarId { get; set; }
        public string contestantId { get; set; }
        public string playerId { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_HeadToHeadSummary")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableHeadToHeadSummary
    {
        public int id { get; set; }
        public string matchId { get; set; }
        public int homeWin { get; set; }
        public int awayWin { get; set; }
        public int draw { get; set; }
        public int homeScore { get; set; }
        public int awayScore { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_HeadToHeadMatch")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableHeadToHeadMatches
    {
        public int id { get; set; }
        public string matchId { get; set; }
        public string hthMatchId { get; set; }
        public string teamAId { get; set; }
        public string teamAName { get; set; }
        public string teamBId { get; set; }
        public string teamBName { get; set; }
        public int homeScore { get; set; }
        public int awayScore { get; set; }
        public DateTime? matchDate { get; set; }
        public string competitionId { get; set; }
        public string tcalendarId { get; set; }
    }

    [PetaPoco.TableName("SDCommonMatch_Form")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableMatchForm
    {
        public int id { get; set; }
        public string matchId { get; set; }
        public string contestantId { get; set; }
        public string hthMatchId { get; set; }
        public string teamAId { get; set; }
        public string teamAName { get; set; }
        public string teamBId { get; set; }
        public string teamBName { get; set; }
        public int homeScore { get; set; }
        public int awayScore { get; set; }
        public DateTime? matchDate { get; set; }
        public string competitionId { get; set; }
        public string tcalendarId { get; set; }
    }

    [PetaPoco.TableName("SDCommon_Ranking")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableRanking
    {
        public string id { get; set; }
        public string type { get; set; }
        public string playerId { get; set; }
        public string contestantId { get; set; }
        public int value { get; set; }
        public int rank { get; set; }
        public string competitionId { get; set; }
        public string tcalendarId { get; set; }
    }
    #endregion

    #region MAL Table
    [PetaPoco.TableName("MALSoccer_Team")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableMALTeam
    {
        public int id { get; set; }
        public string newId { get; set; }
    }

    [PetaPoco.TableName("MALSoccer_Venue")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableMALVenue
    {
        public int id { get; set; }
        public string newId { get; set; }
    }

    [PetaPoco.TableName("MALSoccer_Match")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableMALMatch
    {
        public int id { get; set; }
        public string newId { get; set; }
    }

    [PetaPoco.TableName("MALSoccer_Player")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableMALPlayer
    {
        public int id { get; set; }
        public string newId { get; set; }
    }

    [PetaPoco.TableName("MALSoccer_Round")]
    [PetaPoco.PrimaryKey("id, tcId", AutoIncrement = false)]
    public class TableMALRound
    {
        public int id { get; set; }
        public string newId { get; set; }
        public string tcId { get; set; }
    }

    [PetaPoco.TableName("MALSoccer_Group")]
    [PetaPoco.PrimaryKey("id, tcId", AutoIncrement = false)]
    public class TableMALGroup
    {
        public string id { get; set; }
        public string newId { get; set; }
        public string tcId { get; set; }
    }
    #endregion

    #region MAL Hockey Table
    [PetaPoco.TableName("MALHockey_Team")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableMALHockeyTeam
    {
        public int id { get; set; }
        public string newId { get; set; }
    }

    [PetaPoco.TableName("MALHockey_Venue")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableMALHockeyVenue
    {
        public int id { get; set; }
        public string newId { get; set; }
    }

    [PetaPoco.TableName("MALHockey_Match")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableMALHockeyMatch
    {
        public int id { get; set; }
        public string newId { get; set; }
    }

    [PetaPoco.TableName("MALHockey_Player")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = false)]
    public class TableMALHockeyPlayer
    {
        public int id { get; set; }
        public string newId { get; set; }
    }

    [PetaPoco.TableName("MALHockey_Round")]
    [PetaPoco.PrimaryKey("id, tcId", AutoIncrement = false)]
    public class TableMALHockeyRound
    {
        public int id { get; set; }
        public string newId { get; set; }
        public string tcId { get; set; }
    }

    [PetaPoco.TableName("MALHockey_Group")]
    [PetaPoco.PrimaryKey("id, tcId", AutoIncrement = false)]
    public class TableMALHockeyGroup
    {
        public string id { get; set; }
        public string newId { get; set; }
        public string tcId { get; set; }
    }

    [PetaPoco.TableName("SDHockeyPerson_CareerStats")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableHockeyPersonCareerStats
    {
        public int id { get; set; }
        public int gamesCareer { get; set; }
        public int gamesTournament { get; set; }
        public int gamesSeason { get; set; }
        public int startingCareer { get; set; }
        public int startingTournament { get; set; }
        public int startingSeason { get; set; }
        public int goalsTotalCareer { get; set; }
        public int goalsTotalTournament { get; set; }
        public int goalsTotalSeason { get; set; }
        public int goalsFieldCareer { get; set; }
        public int goalsFieldTournament { get; set; }
        public int goalsFieldSeason { get; set; }
        public int goalsPenaltyStrokeCareer { get; set; }
        public int goalsPenaltyStrokeTournament { get; set; }
        public int goalsPenaltyStrokeSeason { get; set; }
        public int goalsPenaltyStrokeMissCareer { get; set; }
        public int goalsPenaltyStrokeMissTournament { get; set; }
        public int goalsPenaltyStrokeMissSeason { get; set; }
        public int goalsPenaltyCornerCareer { get; set; }
        public int goalsPenaltyCornerTournament { get; set; }
        public int goalsPenaltyCornerSeason { get; set; }
        public int goalsPenaltyCornerMissCareer { get; set; }
        public int goalsPenaltyCornerMissTournament { get; set; }
        public int goalsPenaltyCornerMissSeason { get; set; }
        public int shotsTotalCareer { get; set; }
        public int shotsTotalTournament { get; set; }
        public int shotsTotalSeason { get; set; }
        public int shotsOnTargetCareer { get; set; }
        public int shotsOnTargetTournament { get; set; }
        public int shotsOnTargetSeason { get; set; }
        public int shotsOffTargetCareer { get; set; }
        public int shotsOffTargetTournament { get; set; }
        public int foulsCareer { get; set; }
        public int foulsTournament { get; set; }
        public int foulsSeason { get; set; }
        public int tacklesCareer { get; set; }
        public int tacklesTournament { get; set; }
        public int tacklesSeason { get; set; }
        public int goalDoublesCareer { get; set; }
        public int goalDoublesTournament { get; set; }
        public int goalDoublesSeason { get; set; }
        public int goalHatTricksCareer { get; set; }
        public int goalHatTricksTournament { get; set; }
        public int goalHatTricksSeason { get; set; }
        public int greenCardsCareer { get; set; }
        public int greenCardsTournament { get; set; }
        public int greenCardsSeason { get; set; }
        public int yellowCardsCareer { get; set; }
        public int yellowCardsTournament { get; set; }
        public int yellowCardsSeason { get; set; }
        public int redCardsCareer { get; set; }
        public int redCardsTournament { get; set; }
        public int redCardsSeason { get; set; }
        public int turnoversCareer { get; set; }
        public int turnoversTournament { get; set; }
        public int turnoversSeason { get; set; }
        public int mistrapCareer { get; set; }
        public int mistrapTournament { get; set; }
        public int mistrapSeason { get; set; }
        public int eliminationPassesCareer { get; set; }
        public int eliminationPassesTournament { get; set; }
        public int eliminationPassesSeason { get; set; }
        public int penetrationsTotalCareer { get; set; }
        public int penetrationsTotalTournament { get; set; }
        public int penetrationsTotalSeason { get; set; }
        public int penetrationsLeftCareer { get; set; }
        public int penetrationsLeftTournament { get; set; }
        public int penetrationsLeftSeason { get; set; }
        public int penetrationsCenterCareer { get; set; }
        public int penetrationsCenterTournament { get; set; }
        public int penetrationsCenterSeason { get; set; }
        public int penetrationsRightCareer { get; set; }
        public int penetrationsRightTournament { get; set; }
        public int penetrationsRightSeason { get; set; }
        public int dragFlickDirectSuccessCareer { get; set; }
        public int dragFlickDirectSuccessTournament { get; set; }
        public int dragFlickDirectSuccessSeason { get; set; }
        public int dragFlickDirectFailCareer { get; set; }
        public int dragFlickDirectFailTournament { get; set; }
        public int dragFlickDirectFailSeason { get; set; }
        public int dragFlickIndirectSuccessCareer { get; set; }
        public int dragFlickIndirectSuccessTournament { get; set; }
        public int dragFlickIndirectSuccessSeason { get; set; }
        public int dragFlickIndirectFailCareer { get; set; }
        public int dragFlickIndirectFailTournament { get; set; }
        public int dragFlickIndirectFailSeason { get; set; }
        public int internationalGamesCareer { get; set; }
        public string competitionId { get; set; }
        public string tournamentCalendarId { get; set; }
        public string contestantId { get; set; }
        public string playerId { get; set; }
    }

    [PetaPoco.TableName("SDHockeyTeam_Stats")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class TableHockeyTeamStats
    {
        public int id { get; set; }
        public int hockeyTeamId { get; set; }
        public string contestantId { get; set; }
        public string competitionId { get; set; }
        public string calendarId { get; set; }
        public int Matches_total { get; set; }
        public int Matches_total_home { get; set; }
        public int Matches_total_away { get; set; }
        public int Matches_total_played { get; set; }
        public int Matches_played_home { get; set; }
        public int Matches_played_away { get; set; }
        public int Matches_won_total { get; set; }
        public int Matches_won_home { get; set; }
        public int Matches_won_away { get; set; }
        public int Matches_draw_total { get; set; }
        public int Matches_draw_home { get; set; }
        public int Matches_draw_away { get; set; }
        public int Matches_lost_total { get; set; }
        public int Matches_lost_home { get; set; }
        public int Matches_lost_away { get; set; }
        public int Highest_win_home { get; set; }
        public int Highest_win_away { get; set; }
        public int Highest_lost_home { get; set; }
        public int Highest_lost_away { get; set; }
        public int Total_goals_for_and_against { get; set; }
        public int Total_goals_for_and_against_home { get; set; }
        public int Total_goals_for_and_against_away { get; set; }
        public int Total_goals_per_match { get; set; }
        public int Total_goals_per_match_home { get; set; }
        public int Total_goals_per_match_away { get; set; }
        public int Goals_for_total { get; set; }
        public int Goals_for_total_home { get; set; }
        public int Goals_for_total_away { get; set; }
        public int Goals_for_per_match_total { get; set; }
        public int Goals_for_per_match_home { get; set; }
        public int Goals_for_per_match_away { get; set; }
        public int Goals_against_total { get; set; }
        public int Goals_against_total_home { get; set; }
        public int Goals_against_total_away { get; set; }
        public int Goals_against_per_match_total { get; set; }
        public int Goals_against_per_match_home { get; set; }
        public int Goals_against_per_match_away { get; set; }
        public int No_goals_against_total { get; set; }
        public int No_goals_against_home { get; set; }
        public int No_goals_against_away { get; set; }
    }
    #endregion
}
