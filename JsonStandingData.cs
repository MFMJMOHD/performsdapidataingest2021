﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016
{
    public class JsonStandingData
    {
        public Sport sport { get; set; }
        public Ruleset ruleset { get; set; }
        public Competition competition { get; set; }
        public TournamentCalendar tournamentCalendar { get; set; }
        public List<Stage> stage { get; set; }
        public string lastUpdated { get; set; }

        public class Sport
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Ruleset
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Competition
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class TournamentCalendar
        {
            public string id { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string name { get; set; }
        }

        public class Ranking
        {
            public int rank { get; set; }
            public int lastRank { get; set; }
            public string contestantId { get; set; }
            public string contestantName { get; set; }
            public int points { get; set; }
            public int matchesPlayed { get; set; }
            public int matchesWon { get; set; }
            public int matchesLost { get; set; }
            public int matchesDrawn { get; set; }
            public int goalsFor { get; set; }
            public int goalsAgainst { get; set; }
            public string goaldifference { get; set; }
        }

        public class Division
        {
            public string type { get; set; }
            public string groupId { get; set; }
            public string groupName { get; set; }
            public List<Ranking> ranking { get; set; }
        }

        public class Stage
        {
            public string id { get; set; }
            public string name { get; set; }
            public List<Division> division { get; set; }
        }
    }
}
