﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace PerformSDAPIDataIngest2016
{
    public class IngestMatchDetail : IngestionBase
    {
        private string performUrl = ConfigurationManager.AppSettings["performUrlMatchStats"];
        private string performLiveUrl = ConfigurationManager.AppSettings["performUrlMatch"];
        private string performCompId = ConfigurationManager.AppSettings["performCompId"];
        private string performTcId = ConfigurationManager.AppSettings["performTcId"];
        private string performUuId = ConfigurationManager.AppSettings["performUrlUuId"];
        private string performMatchStatus = ConfigurationManager.AppSettings["performMatchStatus"];
        private string performMatchDetailCount = ConfigurationManager.AppSettings["performMatchDetailCount"];
        private string enableDateRange = ConfigurationManager.AppSettings["enableDateRange"];
        private DatabaseManager dm = new DatabaseManager();
        Helper.StatsPerformHelper _performHelper;
        public IngestMatchDetail()
        {
            _performHelper = new Helper.StatsPerformHelper();
        }
        public void DoIngest()
        {
            logInfo("Starting ingestion...");

            var tcData = dm.getTournamentCalendar(performCompId);
            if (tcData != null)
            {
                var playingMatchList = new List<TableMatchInfo>();
                var tInt = 0;

                var matchCount = int.TryParse(performMatchDetailCount, out tInt) ? tInt : 0;
                string liveUrl = "";
                if (string.IsNullOrEmpty(enableDateRange))
                {
                    liveUrl = performLiveUrl + "/{0}?tmcl={1}&status={2}&live=yes&_pgNm=1&_pgSz={3}&_fmt=json&_rt=b";
                }
                else
                {
                    if (enableDateRange == "yes")
                    {
                        //2021-04-18T00:00:00Z - example of time formate
                        //DateTime previousDate = DateTime.UtcNow.AddDays(-2);
                        //DateTime futureDate = DateTime.UtcNow.AddDays(2);
                        //liveUrl = performLiveUrl + "/{0}?tmcl={1}&status={2}&live=yes&_pgNm=1&_pgSz={3}&_fmt=json&_rt=b&mt.mDt=[" + previousDate.ToString("yyyy-MM-ddT00:00:00Z") + " TO "
                        //    + futureDate.ToString("yyyy-MM-ddT11:59:59Z") + "]";
                        liveUrl = performLiveUrl + "/{0}?tmcl={1}&status={2}&live=yes&_pgNm=1&_pgSz={3}&_fmt=json&_rt=b&mt.mDt=[" + PerformIngestionConfig.FirstDateString + " TO "
                            + PerformIngestionConfig.SecondDateString + "]";
                    }
                    else
                        liveUrl = performLiveUrl + "/{0}?tmcl={1}&status={2}&live=yes&_pgNm=1&_pgSz={3}&_fmt=json&_rt=b";
                }

                if (matchCount == 0)
                    matchCount = 1000;

                logInfo(string.Format("Match count : {0}", matchCount));

                if (string.IsNullOrEmpty(performTcId))
                {
                    liveUrl = String.Format(liveUrl, performUuId, tcData.id, performMatchStatus, matchCount);
                    playingMatchList = dm.getMatchesByStatus(tcData.id, "playing");
                }
                else
                {
                    liveUrl = String.Format(liveUrl, performUuId, performTcId, performMatchStatus, matchCount);
                    playingMatchList = dm.getMatchesByStatus(performTcId, "playing");
                }

                var matchesLive = ReadcompListFromUrlLive(liveUrl);
                if (matchesLive.match != null && matchesLive.match.Count > 0)
                {
                    Console.WriteLine("matchesLive.match count: " + matchesLive.match.Count.ToString());
                    int count = 0;
                    foreach (var m in matchesLive.match)
                    {
                        count++;
                        Console.WriteLine("##########################################################");
                        Console.WriteLine("MATCHES RECORD NUMBER: " + count.ToString()+"/"+ matchesLive.match.Count.ToString());
                        string dataUrl = performUrl + "/{0}/{1}?detailed=yes&_fmt=json&_rt=b";
                        dataUrl = String.Format(dataUrl, performUuId, m.matchInfo.id);
                        IngestUrl(dataUrl);

                        var selectedPlayingMatch = playingMatchList.Where(p => p.id == m.matchInfo.id).FirstOrDefault();
                        if (selectedPlayingMatch != null)
                        {
                            playingMatchList.Remove(selectedPlayingMatch);
                        }
                    }
                }
                else
                    logInfo("No Match to ingest.");

                logInfo("Ingest Match that still in playing status");
                if (playingMatchList != null && playingMatchList.Count > 0)
                {
                    foreach (var pm in playingMatchList)
                    {
                        string dataUrl = performUrl + "/{0}/{1}?detailed=yes&_fmt=json&_rt=b";
                        dataUrl = String.Format(dataUrl, performUuId, pm.id);
                        IngestUrl(dataUrl);
                    }
                }
                else
                    logInfo("No Match to ingest.");
            }

            //string dataUrl = performUrl + "/{0}/{1}?detailed=yes&_fmt=json&_rt=b";
            //dataUrl = String.Format(dataUrl, performUuId, "aewx6iswysxjpgzdh6evhnjs9");
            //IngestUrl(dataUrl);

            logInfo("Completed ingestion.");
        }

        public void IngestUrl(string url)
        {
            var tDate = DateTime.MinValue;
            var tInt = 0;
            var tDouble = 0.0;

            try
            {
                var md = ReadcompListFromUrl(url);
                if (md != null)
                {

                    if (md.liveData != null)
                    {
                        var mdDetails = md.liveData.matchDetails;
                        if (mdDetails != null)
                        {
                            var matchData = dm.getMatch(md.matchInfo.id);
                            if (matchData == null)
                                return;

                            logInfo(mdDetails.matchStatus);

                            tDate = ProcessTableMatchInfo(ref tInt, md, mdDetails);
                        }

                        if (md.liveData.goal != null)
                        {
                            int mainMin = 0;
                            int extraMin = 0;

                            foreach (var g in md.liveData.goal)
                            {
                                try
                                {
                                    ProcessMatchGoal(out tDate, md, out mainMin, out extraMin, g);

                                    if (!string.IsNullOrEmpty(g.assistPlayerId))
                                    {
                                        ProcessMatchAssist(out tDate, md, out mainMin, out extraMin, g);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logError(ex.StackTrace);
                                }
                            }
                        }

                        if (md.liveData.missedPen != null)
                        {
                            int mainMin = 0;
                            int extraMin = 0;

                            foreach (var g in md.liveData.missedPen)
                            {
                                try
                                {
                                    GetEventTime(g.timeMin, g.periodId, out mainMin, out extraMin);
                                    var tmg = new TableMatchGoals();
                                    tmg.optaEventId = g.optaEventId;
                                    tmg.type = g.type;
                                    tmg.playerId = g.playerId;
                                    tmg.timeMin = mainMin;
                                    tmg.timeMinPlus = extraMin;
                                    tmg.periodId = g.periodId;
                                    tmg.lastUpdated = DateTime.TryParse(g.lastUpdated, out tDate) ? tDate : DateTime.MinValue;
                                    tmg.contestantId = g.contestantId;
                                    tmg.matchId = md.matchInfo.id;
                                    var Id = dm.InsertOrUpdateMatchGoals(tmg);
                                }
                                catch (Exception ex)
                                {
                                    logError(ex.StackTrace);
                                }
                            }
                        }

                        if (md.liveData.penaltyShot != null)
                        {
                            tDate = ProcessPenaltyInfo(tDate, md);
                        }

                        if (md.liveData.card != null)
                        {
                            int mainMin = 0;
                            int extraMin = 0;

                            ProcessMatchBooking(ref tDate, md, ref mainMin, ref extraMin);
                        }

                        if (md.liveData.substitute != null)
                        {
                            int mainMin = 0;
                            int extraMin = 0;

                            ProcessMatchSub(ref tDate, md, ref mainMin, ref extraMin);
                        }

                        if (PerformIngestionConfig.EnableMatchDetailLineUp)
                        {
                            if (md.liveData.lineUp != null)
                            {
                                Console.WriteLine("md.livedata.lineup count: " + md.liveData.lineUp.Count.ToString());

                                foreach (var l in md.liveData.lineUp)
                                {
                                    ProcessMatchLineUp(md, l);

                                    //match manager
                                    if (l.teamOfficial != null)
                                    {
                                        var tml = new TableMatchLineups();
                                        tml.optaEventId = 0;
                                        tml.playerId = l.teamOfficial.id;
                                        tml.shirtNumber = 0;
                                        tml.position = l.teamOfficial.type;
                                        tml.positionSide = "";
                                        tml.contestantId = l.contestantId;
                                        tml.matchId = md.matchInfo.id;
                                        dm.InsertOrUpdateMatchLineups(tml);
                                    }

                                    //match team stats
                                    if (l.stat != null)
                                    {
                                        foreach (var ts in l.stat)
                                        {
                                            ProcessMatchTeamStats(tDouble, md, l, ts);
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                
                }
            }
            catch (Exception ex)
            {
                logError(ex.StackTrace);
            }
        }

        private void ProcessMatchTeamStats(double tDouble, JsonMatchStatData md, JsonMatchStatData.LineUp l, JsonMatchStatData.Stat ts)
        {
            try
            {
                if (ts.type == "possessionPercentage" || ts.type == "goals" || ts.type == "totalScoringAtt" || ts.type == "ontargetScoringAtt" ||
                    ts.type == "blockedScoringAtt" || ts.type == "shotOffTarget" || ts.type == "wonCorners" || ts.type == "fkFoulWon" ||
                    ts.type == "fkFoulLost" || ts.type == "totalOffside" || ts.type == "penaltyWon" || ts.type == "totalYellowCard" ||
                    ts.type == "totalRedCard" || ts.type == "subsMade" || ts.type == "formationUsed")
                {
                    var mts = new TableMatchTeamStats();
                    mts.optaEventId = 0;
                    mts.type = ts.type;
                    mts.fh = double.TryParse(ts.fh, out tDouble) ? tDouble : 0;
                    mts.sh = double.TryParse(ts.sh, out tDouble) ? tDouble : 0;
                    mts.value = double.TryParse(ts.value, out tDouble) ? tDouble : 0;
                    mts.contestantId = l.contestantId;
                    mts.matchId = md.matchInfo.id;
                    dm.InsertOrUpdateMatchTeamStats(mts);
                }
            }
            catch (Exception ex)
            {
                logError(ex.StackTrace);
            }
            
        }

        private void ProcessMatchLineUp(JsonMatchStatData md, JsonMatchStatData.LineUp l)
        {
            foreach (var p in l.player)
            {
                try
                {
                    var tml = new TableMatchLineups();
                    tml.optaEventId = 0;
                    tml.playerId = p.playerId;
                    tml.shirtNumber = p.shirtNumber;
                    tml.position = p.position;
                    tml.positionSide = p.positionSide;
                    tml.contestantId = l.contestantId;
                    tml.matchId = md.matchInfo.id;
                    dm.InsertOrUpdateMatchLineups(tml);

                }
                catch (Exception ex)
                {
                    logError(ex.StackTrace);
                }
            }
        }

        private void ProcessMatchSub(ref DateTime tDate, JsonMatchStatData md, ref int mainMin, ref int extraMin)
        {
            foreach (var g in md.liveData.substitute)
            {
                try
                {
                    GetEventTime(g.timeMin, g.periodId, out mainMin, out extraMin);
                    var tms = new TableMatchSubstitutions();
                    tms.optaEventId = 0;
                    tms.playerOnId = g.playerOnId;
                    tms.playerOffId = g.playerOffId;
                    tms.timeMin = mainMin;
                    tms.timeMinPlus = extraMin;
                    tms.periodId = g.periodId;
                    tms.lastUpdated = DateTime.TryParse(g.lastUpdated, out tDate) ? tDate : DateTime.MinValue;
                    tms.contestantId = g.contestantId;
                    tms.matchId = md.matchInfo.id;
                    var Id = dm.InsertOrUpdateMatchSubstitutions(tms);
                }
                catch (Exception ex)
                {
                    logError(ex.StackTrace);
                }
            }
        }

        private void ProcessMatchBooking(ref DateTime tDate, JsonMatchStatData md, ref int mainMin, ref int extraMin)
        {
            foreach (var g in md.liveData.card)
            {
                try
                {
                    GetEventTime(g.timeMin, g.periodId, out mainMin, out extraMin);
                    var tmb = new TableMatchBookings();
                    tmb.optaEventId = g.optaEventId;
                    tmb.type = g.type;
                    tmb.playerId = g.playerId;
                    tmb.timeMin = mainMin;
                    tmb.timeMinPlus = extraMin;
                    tmb.periodId = g.periodId;
                    tmb.lastUpdated = DateTime.TryParse(g.lastUpdated, out tDate) ? tDate : DateTime.MinValue;
                    tmb.contestantId = g.contestantId;
                    tmb.matchId = md.matchInfo.id;
                    var Id = dm.InsertOrUpdateMatchBookings(tmb);
                }
                catch (Exception ex)
                {
                    logError(ex.StackTrace);
                }
            }
        }

        private DateTime ProcessPenaltyInfo(DateTime tDate, JsonMatchStatData md)
        {
            foreach (var g in md.liveData.penaltyShot)
            {
                try
                {
                    var tmps = new TableMatchPenaltyShot();
                    tmps.optaEventId = g.optaEventId;
                    tmps.playerId = g.playerId;
                    tmps.outcome = g.outcome;
                    tmps.teamPenaltyNumber = g.teamPenaltyNumber;
                    tmps.lastUpdated = DateTime.TryParse(g.lastUpdated, out tDate) ? tDate : DateTime.MinValue;
                    tmps.contestantId = g.contestantId;
                    tmps.matchId = md.matchInfo.id;
                    var Id = dm.InsertOrUpdateMatchPenaltyShot(tmps);
                }
                catch (Exception ex)
                {
                    logError(ex.StackTrace);
                }
            }

            return tDate;
        }

        private void ProcessMatchAssist(out DateTime tDate, JsonMatchStatData md, out int mainMin, out int extraMin, JsonMatchStatData.Goal g)
        {
            GetEventTime(g.timeMin, g.periodId, out mainMin, out extraMin);
            var tma = new TableMatchAssists();
            tma.optaEventId = g.optaEventId;
            tma.type = "AST";
            tma.playerId = g.assistPlayerId;
            tma.timeMin = mainMin;
            tma.timeMinPlus = extraMin;
            tma.periodId = g.periodId;
            tma.lastUpdated = DateTime.TryParse(g.lastUpdated, out tDate) ? tDate : DateTime.MinValue;
            tma.contestantId = g.contestantId;
            tma.matchId = md.matchInfo.id;
            dm.InsertOrUpdateMatchAssists(tma);
        }

        private void ProcessMatchGoal(out DateTime tDate, JsonMatchStatData md, out int mainMin, out int extraMin, JsonMatchStatData.Goal g)
        {
            GetEventTime(g.timeMin, g.periodId, out mainMin, out extraMin);
            var tmg = new TableMatchGoals();
            tmg.optaEventId = g.optaEventId;
            tmg.type = g.type;
            tmg.playerId = g.scorerId;
            tmg.timeMin = mainMin;
            tmg.timeMinPlus = extraMin;
            tmg.periodId = g.periodId;
            tmg.lastUpdated = DateTime.TryParse(g.lastUpdated, out tDate) ? tDate : DateTime.MinValue;
            tmg.contestantId = g.contestantId;
            tmg.matchId = md.matchInfo.id;
            dm.InsertOrUpdateMatchGoals(tmg);
        }

        private DateTime ProcessTableMatchInfo(ref int tInt, JsonMatchStatData md, JsonMatchStatData.MatchDetails mdDetails)
        {
            DateTime tDate;
            var m = new TableMatchInfo();
            m.id = md.matchInfo.id;
            m.matchStatus = mdDetails.matchStatus;
            m.winner = mdDetails.winner;
            m.periodId = mdDetails.periodId;
            m.periodCount = mdDetails.period.Count;
            m.aggregateWinnerId = mdDetails.aggregateWinnerId;

            m.lastUpdated = DateTime.TryParse(md.matchInfo.lastUpdated, out tDate) ? tDate : DateTime.MinValue;

            if (mdDetails.matchStatus == "Playing")
            {
                m.matchLengthMin = mdDetails.matchTime;
            }
            else
            {
                m.matchLengthMin = mdDetails.matchLengthMin;
                m.matchLengthSec = mdDetails.matchLengthSec;
            }

            if (mdDetails.scores.ht != null)
            {
                m.scoreHtHome = mdDetails.scores.ht.home;
                m.scoreHtAway = mdDetails.scores.ht.away;
            }

            if (mdDetails.scores.ft != null)
            {
                m.scoreFtHome = mdDetails.scores.ft.home;
                m.scoreFtAway = mdDetails.scores.ft.away;
            }

            if (mdDetails.scores.et != null)
            {
                m.scoreEtHome = mdDetails.scores.et.home;
                m.scoreEtAway = mdDetails.scores.et.away;
            }

            if (mdDetails.scores.pen != null)
            {
                m.scorePenHome = mdDetails.scores.pen.home;
                m.scorePenAway = mdDetails.scores.pen.away;
            }

            if (mdDetails.scores.total != null)
            {
                m.scoreTotHome = mdDetails.scores.total.home;
                m.scoreTotAway = mdDetails.scores.total.away;
            }

            if (mdDetails.scores.aggregate != null)
            {
                m.scoreAggHome = mdDetails.scores.aggregate.home;
                m.scoreAggAway = mdDetails.scores.aggregate.away;
            }

            if (md.liveData.matchDetailsExtra != null)
            {
                m.attendance = int.TryParse(md.liveData.matchDetailsExtra.attendance, out tInt) ? tInt : 0;
                m.weather = md.liveData.matchDetailsExtra.weather;

                if (md.liveData.matchDetailsExtra.matchOfficial != null)
                {
                    foreach (var mo in md.liveData.matchDetailsExtra.matchOfficial)
                    {
                        try
                        {
                            var tmo = new TableMatchOfficials();
                            tmo.optaEventId = 0;
                            tmo.officialId = mo.id;
                            tmo.type = mo.type;
                            tmo.firstName = mo.firstName;
                            tmo.lastName = mo.lastName;
                            tmo.matchId = md.matchInfo.id;
                            var Id = dm.InsertOrUpdateMatchOfficials(tmo);
                        }
                        catch (Exception ex)
                        {
                            logError(ex.StackTrace);
                        }
                    }
                }
            }

            var matchId = dm.InsertOrUpdateMatch(m);
            return tDate;
        }

        public JsonMatchData ReadcompListFromUrlLive(string url)
        {
            // Create a list of articles to loop through later.
            var matchList = new JsonMatchData();

            try
            {
                // Store the response from the Url into a string for parsing.
                string htmlResponse = _performHelper.GetHtmlResponseFromUrl(url);
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                matchList = js.Deserialize<JsonMatchData>(htmlResponse);
            }
            catch (Exception ex)
            {
                logError(ex.Message);
                return matchList;
            }
            return matchList;
        }

        public JsonMatchStatData ReadcompListFromUrl(string url)
        {
            // Create a list of articles to loop through later.
            var matchList = new JsonMatchStatData();

            try
            {
                // Store the response from the Url into a string for parsing.
                string htmlResponse = _performHelper.GetHtmlResponseFromUrl(url);
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                matchList = js.Deserialize<JsonMatchStatData>(htmlResponse);       
            }
            catch (Exception ex)
            {
                logError(ex.Message);
                return matchList;
            }
            return matchList;
        }

        #region Helper
        public void GetEventTime(int min, int periodId, out int mainMin, out int extraMin)
        {
            if (min > 120)
            {
                if (periodId == 4)
                {
                    mainMin = 120;
                    extraMin = min - mainMin;
                }
                else
                {
                    mainMin = min;
                    extraMin = 0;
                }
            }
            else if (min > 105)
            {
                if (periodId == 3)
                {
                    mainMin = 105;
                    extraMin = min - mainMin;
                }
                else
                {
                    mainMin = min;
                    extraMin = 0;
                }
            }
            else if (min > 90)
            {
                if (periodId == 2)
                {
                    mainMin = 90;
                    extraMin = min - mainMin;
                }
                else
                {
                    mainMin = min;
                    extraMin = 0;
                }
            }
            else if (min > 45)
            {
                if (periodId == 1)
                {
                    mainMin = 45;
                    extraMin = min - mainMin;
                }
                else
                {
                    mainMin = min;
                    extraMin = 0;
                }
            }
            else
            {
                mainMin = min;
                extraMin = 0;           
            }
        }

        //public string GetHtmlResponseFromUrl(string url)
        //{
        //    logInfo("Reading from url : " + url);
        //    StringBuilder sb = new StringBuilder();
        //    byte[] buffer = new byte[8192];

        //    try
        //    {
        //        Uri uri = new Uri(url);

        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
        //        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //        Stream respStream = response.GetResponseStream();

        //        string tempString = "";
        //        int count = 0;

        //        do
        //        {
        //            count = respStream.Read(buffer, 0, buffer.Length);

        //            if (count != 0)
        //            {
        //                tempString = Encoding.UTF8.GetString(buffer, 0, count);
        //                sb.Append(tempString);
        //            }

        //        } while (count > 0);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return sb.ToString();
        //}
        #endregion
    }
}
