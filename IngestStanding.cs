﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace PerformSDAPIDataIngest2016
{
    public class IngestStanding : IngestionBase
    {
        private string performUrl = System.Configuration.ConfigurationManager.AppSettings["performUrlStanding"];
        private string performCompId = System.Configuration.ConfigurationManager.AppSettings["performCompId"];
        private string performTcId = System.Configuration.ConfigurationManager.AppSettings["performTcId"];
        private string performUuId = System.Configuration.ConfigurationManager.AppSettings["performUrlUuId"];
        private DatabaseManager dm = new DatabaseManager();
        Helper.StatsPerformHelper _performHelper;
        public IngestStanding()
        {
            _performHelper = new Helper.StatsPerformHelper();
        }
        public void DoIngest()
        {
            logInfo("Starting ingestion...");

            var tcData = dm.getTournamentCalendar(performCompId);
            if (tcData != null)
            {
                string dataUrl = performUrl + "/{0}?tmcl={1}&_fmt=json&_rt=b";
                if (string.IsNullOrEmpty(performTcId))
                    dataUrl = String.Format(dataUrl, performUuId, tcData.id);
                else
                    dataUrl = String.Format(dataUrl, performUuId, performTcId);
                IngestUrl(dataUrl);
            }
            logInfo("Completed ingestion.");
        }

        public void IngestUrl(string url)
        {
            try
            {
                var sd = ReadcompListFromUrl(url);
                if (sd != null)
                {
                    if (sd.stage != null)
                    {
                        var si = new TableStage();
                        si.id = sd.stage.FirstOrDefault().id;
                        si.name = sd.stage.FirstOrDefault().name;
                        var stageId = dm.InsertOrUpdateStage(si);

                        var divList = sd.stage.FirstOrDefault().division;
                        if (divList != null)
                        {
                            foreach (var d in divList.Where(d => d.type == "total" || d.type == "home" || d.type == "away").ToList())
                            {
                                try
                                {
                                    if (d.groupId != null)
                                    {
                                        var di = new TableSeries();
                                        di.id = d.groupId;
                                        di.name = d.groupName;
                                        var seriesId = dm.InsertOrUpdateSeries(di);
                                    }

                                    foreach (var r in d.ranking)
                                    {
                                        try
                                        {
                                            var ri = new TableStanding();
                                            ri.rank = r.rank;
                                            ri.lastRank = r.lastRank;
                                            ri.contestantId = r.contestantId;
                                            ri.matchesPlayed = r.matchesPlayed;
                                            ri.matchesWon = r.matchesWon;
                                            ri.matchesDrawn = r.matchesDrawn;
                                            ri.matchesLost = r.matchesLost;
                                            ri.goalsFor = r.goalsFor;
                                            ri.goalsAgainst = r.goalsAgainst;
                                            ri.goaldifference = r.goaldifference;
                                            ri.points = r.points;
                                            ri.type = d.type;
                                            ri.seriesId = d.groupId;
                                            ri.stageId = sd.stage.FirstOrDefault().id;
                                            ri.tcalendarId = sd.tournamentCalendar.id;
                                            ri.competitionId = sd.competition.id;

                                            var rankId = dm.InsertOrUpdateStanding(ri);
                                        }
                                        catch (Exception ex)
                                        {
                                            logError(ex.StackTrace);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logError(ex.StackTrace);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logError(ex.StackTrace);
            }
        }

        public JsonStandingData ReadcompListFromUrl(string url)
        {
            // Create a list of articles to loop through later.
            var stanList = new JsonStandingData();

            try
            {
                // Store the response from the Url into a string for parsing.
                string htmlResponse = _performHelper.GetHtmlResponseFromUrl(url);
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                stanList = js.Deserialize<JsonStandingData>(htmlResponse);       
            }
            catch (Exception ex)
            {
                logError(ex.Message);
                return stanList;
            }
            return stanList;
        }

        #region Helper
        //public string GetHtmlResponseFromUrl(string url)
        //{
        //    logInfo("Reading from url : " + url);
        //    StringBuilder sb = new StringBuilder();
        //    byte[] buffer = new byte[8192];

        //    try
        //    {
        //        Uri uri = new Uri(url);

        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
        //        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //        Stream respStream = response.GetResponseStream();

        //        string tempString = "";
        //        int count = 0;

        //        do
        //        {
        //            count = respStream.Read(buffer, 0, buffer.Length);

        //            if (count != 0)
        //            {
        //                tempString = Encoding.UTF8.GetString(buffer, 0, count);
        //                sb.Append(tempString);
        //            }

        //        } while (count > 0);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return sb.ToString();
        //}
        #endregion
    }
}
