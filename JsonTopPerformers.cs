﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016
{
    public class JsonTopPerformers
    {
        public DateTime lastUpdated { get; set; }
        public Competition competition { get; set; }
        public TournamentCalendar tournamentCalendar { get; set; }
        public PlayerTopPerformers playerTopPerformers { get; set; }
        public TeamTopPerformers teamTopPerformers { get; set; }

        public class Competition
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class TournamentCalendar
        {
            public string endDate { get; set; }
            public string id { get; set; }
            public string startDate { get; set; }
            public string name { get; set; }
        }

        public class Player
        {
            public string contestantId { get; set; }
            public string contestantName { get; set; }
            public string firstName { get; set; }
            public string id { get; set; }
            public string lastName { get; set; }
            public string matchName { get; set; }
            public int rank { get; set; }
            public int value { get; set; }
        }

        public class Team
        {
            public string id { get; set; }
            public string name { get; set; }
            public int rank { get; set; }
            public int value { get; set; }
        }

        public class Ranking
        {
            public string name { get; set; }
            public List<Player> player { get; set; }
            public List<Team> team { get; set; }
        }

        public class PlayerTopPerformers
        {
            public List<Ranking> ranking { get; set; }
        }

        public class TeamTopPerformers
        {
            public List<Ranking> ranking { get; set; }
        }
    }
}
