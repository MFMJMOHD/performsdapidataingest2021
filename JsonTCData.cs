﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016
{
    public class JsonTCData
    {
        public List<Competition> competition { get; set; }
        public string lastUpdated { get; set; }

        public class TournamentCalendar
        {
            public string id { get; set; }
            public string ocId { get; set; }
            public string name { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string active { get; set; }
            public string lastUpdated { get; set; }
        }

        public class Competition
        {
            public string id { get; set; }
            public string ocId { get; set; }
            public string opId { get; set; }
            public string name { get; set; }
            public string country { get; set; }
            public string countryId { get; set; }
            public List<TournamentCalendar> tournamentCalendar { get; set; }
        }
    }
}
