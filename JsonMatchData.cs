﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016
{
    public class JsonMatchData
    {
        public List<Match> match { get; set; }

        public class Sport
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Ruleset
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Country
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Competition
        {
            public string id { get; set; }
            public string name { get; set; }
            public Country country { get; set; }
        }

        public class Stage
        {
            public string id { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string name { get; set; }
        }

        public class Series
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class TournamentCalendar
        {
            public string id { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string name { get; set; }
        }

        public class Contestant
        {
            public string id { get; set; }
            public string name { get; set; }
            public Country country { get; set; }
            public string position { get; set; }
        }

        public class Venue
        {
            public string id { get; set; }
            public string shortName { get; set; }
            public string longName { get; set; }
        }

        public class MatchInfo
        {
            public string description { get; set; }
            public Sport sport { get; set; }
            public Ruleset ruleset { get; set; }
            public Competition competition { get; set; }
            public Stage stage { get; set; }
            public Series series { get; set; }
            public TournamentCalendar tournamentCalendar { get; set; }
            public List<Contestant> contestant { get; set; }
            public Venue venue { get; set; }
            public string id { get; set; }
            public string date { get; set; }
            public string time { get; set; }
            public string week { get; set; }
            public string lastUpdated { get; set; }
            public string test { get; set; }
        }

        public class MatchDetails
        {
            public string matchStatus { get; set; }
        }

        public class LiveData
        {
            public MatchDetails matchDetails { get; set; }
        }

        public class Match
        {
            public MatchInfo matchInfo { get; set; }
            public LiveData liveData { get; set; }
        }
    }
}
