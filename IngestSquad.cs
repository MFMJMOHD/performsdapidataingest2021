﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace PerformSDAPIDataIngest2016
{
    public class IngestSquad : IngestionBase
    {
        private string performUrl = System.Configuration.ConfigurationManager.AppSettings["performUrlSquad"];
        private string performCompId = System.Configuration.ConfigurationManager.AppSettings["performCompId"];
        private string performTcId = System.Configuration.ConfigurationManager.AppSettings["performTcId"];
        private string performUuId = System.Configuration.ConfigurationManager.AppSettings["performUrlUuId"];
        private DatabaseManager dm = new DatabaseManager();
        Helper.StatsPerformHelper _performHelper;
        public IngestSquad()
        {
            _performHelper = new Helper.StatsPerformHelper();
        }
        public void DoIngest(string enablePersonCareer)
        {
            logInfo("Starting ingestion...");

            var tcData = dm.getTournamentCalendar(performCompId);
            if (tcData != null)
            {
                string dataUrl = performUrl + "/{0}?tmcl={1}&_pgNm=1&_pgSz=500&detailed=yes&_fmt=json&_rt=b";
                if (string.IsNullOrEmpty(performTcId))
                {
                    dataUrl = String.Format(dataUrl, performUuId, tcData.id);
                    IngestUrl(dataUrl, tcData.id, enablePersonCareer);
                }
                else
                {
                    dataUrl = String.Format(dataUrl, performUuId, performTcId);
                    IngestUrl(dataUrl, performTcId, enablePersonCareer);
                }
                
            }

            logInfo("Completed ingestion.");
        }

        public void IngestUrl(string url, string tcId, string enablePersonCareer)
        {
            var tDate = DateTime.MinValue;

            try
            {
                var sd = ReadcompListFromUrl(url);
                if (sd != null)
                {
                    if (sd.squad != null)
                    {   
                        foreach (var t in sd.squad)
                        {
                            try
                            {
                                var ti = new TableContestant();
                                ti.id = t.contestantId;
                                ti.teamType = t.teamType;
                                ti.name = t.contestantName;
                                ti.shortName = t.contestantShortName;
                                ti.clubName = t.contestantClubName;
                                ti.code = t.contestantCode;
                                var teamId = dm.InsertOrUpdateContestant(ti);
                                if (PerformIngestionConfig.EnableSquadDetail)
                                {
                                    if (t.person != null)
                                    {
                                        //delete current contestant squad list by constestantid and tcid
                                        var result = dm.DeleteContestantSquad(t.contestantId, tcId);
                                        logInfo(string.Format("Deleting result:{0}", result));

                                        foreach (var p in t.person)
                                        {
                                            try
                                            {
                                                TablePerson pi;
                                                ProcessPersonInSquad(out tDate, p, out pi);

                                                var cs = new TableContestantSquad();
                                                cs.contestantId = t.contestantId;
                                                cs.playerId = p.id;
                                                cs.tcalendarId = tcId;
                                                var contSquadId = dm.InsertOrUpdateContestantSquad(cs);

                                                if (enablePersonCareer == "yes")
                                                {
                                                    var pc = new IngestPersonCareer();
                                                    pc.DoIngest(pi.id);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                logError(ex.StackTrace);
                                            }
                                        }
                                    }
                                }
                                
                            }
                            catch (Exception ex)
                            {
                                logError(ex.StackTrace);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logError(ex.StackTrace);
            }
        }

        private void ProcessPersonInSquad(out DateTime tDate, JsonSquadData.Person p, out TablePerson pi)
        {
            pi = new TablePerson();
            pi.id = p.id;
            pi.type = p.type;
            pi.firstName = p.firstName;
            pi.middleName = p.middleName;
            pi.lastName = p.lastName;
            pi.matchName = p.matchName;
            pi.dateOfBirth = DateTime.TryParse(p.dateOfBirth, out tDate) ? tDate : DateTime.MinValue;
            pi.placeOfBirth = p.placeOfBirth;
            pi.countryOfBirthId = p.countryOfBirthId;
            pi.countryOfBirth = p.countryOfBirth;
            pi.height = p.height;
            pi.weight = p.weight;
            pi.foot = p.foot;
            pi.status = p.status;
            pi.nationalityId = p.nationalityId;
            pi.position = p.position;
            pi.shirtNumber = p.shirtNumber;
            dm.InsertOrUpdatePerson(pi);
        }

        public JsonSquadData ReadcompListFromUrl(string url)
        {
            // Create a list of articles to loop through later.
            var squadList = new JsonSquadData();

            try
            {
                // Store the response from the Url into a string for parsing.
                string htmlResponse = _performHelper.GetHtmlResponseFromUrl(url);
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                squadList = js.Deserialize<JsonSquadData>(htmlResponse);       
            }
            catch (Exception ex)
            {
                logError(ex.Message);
                return squadList;
            }
            return squadList;
        }

        #region Helper
        //public string GetHtmlResponseFromUrl(string url)
        //{
        //    logInfo("Reading from url : " + url);
        //    StringBuilder sb = new StringBuilder();
        //    byte[] buffer = new byte[8192];

        //    try
        //    {
        //        Uri uri = new Uri(url);

        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
        //        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //        Stream respStream = response.GetResponseStream();

        //        string tempString = "";
        //        int count = 0;

        //        do
        //        {
        //            count = respStream.Read(buffer, 0, buffer.Length);

        //            if (count != 0)
        //            {
        //                tempString = Encoding.UTF8.GetString(buffer, 0, count);
        //                sb.Append(tempString);
        //            }

        //        } while (count > 0);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return sb.ToString();
        //}
        #endregion
    }
}
