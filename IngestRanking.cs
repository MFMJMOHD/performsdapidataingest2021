﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace PerformSDAPIDataIngest2016
{
    public class IngestRanking : IngestionBase
    {
        private string performUrl = System.Configuration.ConfigurationManager.AppSettings["performUrlRanking"];
        private string performCompId = System.Configuration.ConfigurationManager.AppSettings["performCompId"];
        private string performTcId = System.Configuration.ConfigurationManager.AppSettings["performTcId"];
        private string performUuId = System.Configuration.ConfigurationManager.AppSettings["performUrlUuId"];
        private DatabaseManager dm = new DatabaseManager();
        Helper.StatsPerformHelper _performHelper;
        public IngestRanking()
        {
            _performHelper = new Helper.StatsPerformHelper();
        }
        public void DoIngest()
        {
            logInfo("Starting ingestion...");

            var tcData = dm.getTournamentCalendar(performCompId);
            if (tcData != null)
            {
                string dataUrl = performUrl + "/{0}?tmcl={1}&_fmt=json&_rt=b";
                if (string.IsNullOrEmpty(performTcId))
                    dataUrl = String.Format(dataUrl, performUuId, tcData.id);
                else
                    dataUrl = String.Format(dataUrl, performUuId, performTcId);
                IngestUrl(dataUrl);
            }
            logInfo("Completed ingestion.");
        }

        public void IngestUrl(string url)
        {
            try
            {
                var sd = ReadcompListFromUrl(url);
                if (sd != null)
                {
                    if (sd.playerTopPerformers != null)
                    {
                        if (sd.playerTopPerformers.ranking != null)
                        {
                            bool result;
                            logInfo(string.Format("Deleting old ranking compId:{0} tcid:{1}", sd.competition.id, sd.tournamentCalendar.id));
                            result = dm.DeleteRanking(sd.competition.id, sd.tournamentCalendar.id);
                            logInfo(string.Format("Deleting result:{0}", result));

                            if (result)
                            {
                                foreach (var rt in sd.playerTopPerformers.ranking)
                                {
                                    switch (rt.name.ToLower())
                                    {
                                        case "goals":
                                            logInfo("Goal Ranking.");
                                            foreach (var g in rt.player)
                                            {
                                                try
                                                {
                                                    var gr = new TableRanking();
                                                    gr.type = "G";
                                                    gr.playerId = g.id;
                                                    gr.contestantId = g.contestantId;
                                                    gr.value = g.value;
                                                    gr.rank = g.rank;
                                                    gr.competitionId = sd.competition.id;
                                                    gr.tcalendarId = sd.tournamentCalendar.id;

                                                    var rankingId = dm.InsertRanking(gr);
                                                }
                                                catch (Exception ex)
                                                {
                                                    logError(ex.StackTrace);
                                                }
                                            }
                                            break;

                                        case "yellowcards":
                                            logInfo("Yellow Card Ranking.");
                                            foreach (var g in rt.player)
                                            {
                                                try
                                                {
                                                    var gr = new TableRanking();
                                                    gr.type = "YC";
                                                    gr.playerId = g.id;
                                                    gr.contestantId = g.contestantId;
                                                    gr.value = g.value;
                                                    gr.rank = g.rank;
                                                    gr.competitionId = sd.competition.id;
                                                    gr.tcalendarId = sd.tournamentCalendar.id;

                                                    var rankingId = dm.InsertRanking(gr);
                                                }
                                                catch (Exception ex)
                                                {
                                                    logError(ex.StackTrace);
                                                }
                                            }
                                            break;

                                        case "redcards":
                                            logInfo("Red Card Ranking.");
                                            foreach (var g in rt.player)
                                            {
                                                try
                                                {
                                                    var gr = new TableRanking();
                                                    gr.type = "RC";
                                                    gr.playerId = g.id;
                                                    gr.contestantId = g.contestantId;
                                                    gr.value = g.value;
                                                    gr.rank = g.rank;
                                                    gr.competitionId = sd.competition.id;
                                                    gr.tcalendarId = sd.tournamentCalendar.id;

                                                    var rankingId = dm.InsertRanking(gr);
                                                }
                                                catch (Exception ex)
                                                {
                                                    logError(ex.StackTrace);
                                                }
                                            }
                                            break;

                                        case "assists":
                                            logInfo("Assists Ranking.");
                                            foreach (var g in rt.player)
                                            {
                                                try
                                                {
                                                    var gr = new TableRanking();
                                                    gr.type = "AST";
                                                    gr.playerId = g.id;
                                                    gr.contestantId = g.contestantId;
                                                    gr.value = g.value;
                                                    gr.rank = g.rank;
                                                    gr.competitionId = sd.competition.id;
                                                    gr.tcalendarId = sd.tournamentCalendar.id;

                                                    var rankingId = dm.InsertRanking(gr);
                                                }
                                                catch (Exception ex)
                                                {
                                                    logError(ex.StackTrace);
                                                }
                                            }
                                            break;

                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                logError(ex.StackTrace);
            }
        }

        public JsonTopPerformers ReadcompListFromUrl(string url)
        {
            // Create a list of articles to loop through later.
            var stanList = new JsonTopPerformers();

            try
            {
                // Store the response from the Url into a string for parsing.
                string htmlResponse = _performHelper.GetHtmlResponseFromUrl(url);
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                stanList = js.Deserialize<JsonTopPerformers>(htmlResponse);       
            }
            catch (Exception ex)
            {
                logError(ex.Message);
                return stanList;
            }
            return stanList;
        }

        #region Helper
        //public string GetHtmlResponseFromUrl(string url)
        //{
        //    logInfo("Reading from url : " + url);
        //    StringBuilder sb = new StringBuilder();
        //    byte[] buffer = new byte[8192];

        //    try
        //    {
        //        Uri uri = new Uri(url);

        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
        //        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //        Stream respStream = response.GetResponseStream();

        //        string tempString = "";
        //        int count = 0;

        //        do
        //        {
        //            count = respStream.Read(buffer, 0, buffer.Length);

        //            if (count != 0)
        //            {
        //                tempString = Encoding.UTF8.GetString(buffer, 0, count);
        //                sb.Append(tempString);
        //            }

        //        } while (count > 0);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return sb.ToString();
        //}
        #endregion
    }
}
