﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016
{
    public class JsonMatchPreviewData
    {
        public MatchInfo matchInfo { get; set; }
        public PreviousMeetings previousMeetings { get; set; }
        public PreviousMeetingsAnyComp previousMeetingsAnyComp { get; set; }
        public List<Form> form { get; set; }
        public List<FormAnyComp> formAnyComp { get; set; }

        public class Sport
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Ruleset
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Country
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Competition
        {
            public string id { get; set; }
            public string name { get; set; }
            public Country country { get; set; }
        }

        public class TournamentCalendar
        {
            public string id { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string name { get; set; }
        }

        public class Contestant
        {
            public string id { get; set; }
            public string name { get; set; }
            public Country country { get; set; }
            public string position { get; set; }
        }

        public class Venue
        {
            public string id { get; set; }
            public string shortName { get; set; }
            public string longName { get; set; }
        }

        public class MatchInfo
        {
            public string description { get; set; }
            public Sport sport { get; set; }
            public Ruleset ruleset { get; set; }
            public Competition competition { get; set; }
            public TournamentCalendar tournamentCalendar { get; set; }
            public List<Contestant> contestant { get; set; }
            public Venue venue { get; set; }
            public string id { get; set; }
            public string date { get; set; }
            public string time { get; set; }
            public string lastUpdated { get; set; }
        }

        public class Contestants
        {
            public string homeContestantId { get; set; }
            public string homeContestantName { get; set; }
            public string awayContestantId { get; set; }
            public string awayContestantName { get; set; }
            public string homeScore { get; set; }
            public string awayScore { get; set; }
        }

        public class Match
        {
            public Contestants contestants { get; set; }
            public List<object> goal { get; set; }
            public string id { get; set; }
            public string date { get; set; }
            public string competitionId { get; set; }
            public string competitionName { get; set; }
            public string tournamentCalendarId { get; set; }
            public string tournamentCalendarName { get; set; }
        }

        public class PreviousMeetings
        {
            public int homeContestantWins { get; set; }
            public int awayContestantWins { get; set; }
            public int draws { get; set; }
            public int homeContestantGoals { get; set; }
            public int awayContestantGoals { get; set; }
            public List<Match> match { get; set; }
        }

        public class PreviousMeetingsAnyComp
        {
            public int homeContestantWins { get; set; }
            public int awayContestantWins { get; set; }
            public int draws { get; set; }
            public int homeContestantGoals { get; set; }
            public int awayContestantGoals { get; set; }
            public List<Match> match { get; set; }
        }

        public class Form
        {
            public string contestantId { get; set; }
            public string lastSix { get; set; }
            public List<Match> match { get; set; }
        }

        public class FormAnyComp
        {
            public string contestantId { get; set; }
            public string lastSix { get; set; }
            public List<Match> match { get; set; }
        }
    }
}
