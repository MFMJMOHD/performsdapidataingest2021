﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using PerformSDAPIDataIngest2016.Helper;

namespace PerformSDAPIDataIngest2016
{
    public class IngestMatch : IngestionBase
    {
        private string performUrl = ConfigurationManager.AppSettings["performUrlMatch"];
        private string performPreviewUrl = ConfigurationManager.AppSettings["performUrlMatchPreview"];
        private string performCompId = ConfigurationManager.AppSettings["performCompId"];
        private string performTcId = ConfigurationManager.AppSettings["performTcId"];
        private string performUuId = ConfigurationManager.AppSettings["performUrlUuId"];
        private string enableDateRange = ConfigurationManager.AppSettings["enableDateRange"];
        StatsPerformHelper _performHelper;
        //private DatabaseManager dm = new DatabaseManager(_pagerduty);
        private DatabaseManager dm;

        public IngestMatch()
        {
            _performHelper = new StatsPerformHelper();
        }
        public void DoIngest()
        {
            dm = new DatabaseManager();
            logInfo("Starting ingestion...");

            var tcData = dm.getTournamentCalendar(performCompId);
            if (tcData != null)
            {
                string dataUrl;
                if (string.IsNullOrEmpty(enableDateRange))
                {
                    dataUrl = performUrl + "/{0}?tmcl={1}&_pgNm=1&_pgSz=1000&live=yes&_fmt=json&_rt=b";
                }
                else
                {
                    if (enableDateRange == "yes")
                    {
                        dataUrl = performUrl + "/{0}?tmcl={1}&_pgNm=1&_pgSz=1000&live=yes&_fmt=json&_rt=b&mt.mDt=[" + PerformIngestionConfig.FirstDateString + " TO "
                           + PerformIngestionConfig.SecondDateString + "]";
                    }
                    else
                    {
                        dataUrl = performUrl + "/{0}?tmcl={1}&_pgNm=1&_pgSz=1000&live=yes&_fmt=json&_rt=b";
                    }
                }
               

                if (string.IsNullOrEmpty(performTcId))
                    dataUrl = String.Format(dataUrl, performUuId, tcData.id);
                else
                    dataUrl = String.Format(dataUrl, performUuId, performTcId);
                IngestUrl(dataUrl);
            }

            logInfo("Completed ingestion.");
        }

        public void IngestUrl(string url)
        {
          
            var tDate = DateTime.MinValue;
            var tInt = 0;

            try
            {
                var td = ReadcompListFromUrl(url);
                if (td != null)
                {
                    var matchList = td.match;
                    if (matchList != null && matchList.Count > 0)
                    {
                        Console.WriteLine("Total match count="+matchList.Count.ToString());
                        foreach (var m in matchList)
                        {
                            try
                            {
                                var mi = m.matchInfo;
                                var tmi = new TableMatchInfo();

                                var sportId = "";
                                if (mi.sport != null)
                                {
                                    var ts = new TableSport();
                                    ts.id = mi.sport.id;
                                    ts.name = mi.sport.name;

                                    sportId = dm.InsertOrUpdateSport(ts);
                                    tmi.sportId = sportId;
                                }

                                var ruleSetId = "";
                                if (mi.ruleset != null)
                                {
                                    var trs = new TableRuleset();
                                    trs.id = mi.ruleset.id;
                                    trs.name = mi.ruleset.name;

                                    ruleSetId = dm.InsertOrUpdateRuleSet(trs);
                                    tmi.ruleSetId = ruleSetId;
                                }

                                var stageId = "";
                                if (mi.stage != null)
                                {
                                    var tst = new TableStage();
                                    tst.id = mi.stage.id;
                                    tst.name = mi.stage.name;
                                    tst.startDate = DateTime.TryParse(mi.stage.startDate, out tDate) ? tDate : DateTime.MinValue;
                                    tst.endDate = DateTime.TryParse(mi.stage.endDate, out tDate) ? tDate : DateTime.MinValue;

                                    stageId = dm.InsertOrUpdateStage(tst);
                                    tmi.stageId = stageId;
                                }

                                var seriesId = "";
                                if (mi.series != null)
                                {
                                    var tse = new TableSeries();
                                    tse.id = mi.series.id;
                                    tse.name = mi.series.name;

                                    seriesId = dm.InsertOrUpdateSeries(tse);
                                    tmi.seriesId = seriesId;
                                }

                                var venueId = "";
                                if (mi.venue != null)
                                {
                                    var tv = new TableVenue();
                                    tv.id = mi.venue.id;
                                    tv.name = mi.venue.shortName;
                                    tv.longName = mi.venue.longName;

                                    venueId = dm.InsertOrUpdateVenue(tv);
                                    tmi.venueId = venueId;
                                }

                                if (mi.contestant != null)
                                {
                                    foreach (var c in mi.contestant)
                                    {
                                        var tct = new TableContestant();
                                        tct.id = c.id;
                                        tct.clubName = c.name;
                                        tct.countryId = c.country.id;

                                        var teamId = dm.InsertOrUpdateContestant(tct);

                                        if (c.position == "home")
                                            tmi.teamAId = c.id;
                                        else if (c.position == "away")
                                            tmi.teamBId = c.id;
                                    }
                                }
                                

                                tmi.id = mi.id;
                                tmi.description = mi.description;
                                tmi.date = DateTime.Parse(mi.date.Replace("Z", "") + " " + mi.time.Replace("Z", ""));
                                tmi.week = mi.week;
                                tmi.competitionId = mi.competition.id;
                                tmi.tcalendarId = mi.tournamentCalendar.id;
                                tmi.lastUpdated = DateTime.TryParse(mi.lastUpdated, out tDate) ? tDate : DateTime.MinValue;

                                if (m.liveData.matchDetails.matchStatus != null)
                                {
                                    tmi.matchStatus = m.liveData.matchDetails.matchStatus;
                                }

                                var matchId = dm.InsertOrUpdateMatch(tmi);

                                if (tmi.matchStatus != "Played")
                                {
                                    //match preview
                                    string dataPreviewUrl = performPreviewUrl + "/{0}/{1}?_fmt=json&_rt=b";
                                    dataPreviewUrl = String.Format(dataPreviewUrl, performUuId, mi.id);

                                    var mp = ReadcompListFromUrlMatchPreview(dataPreviewUrl);
                                    if (mp != null)
                                    {
                                        if (mp.previousMeetingsAnyComp != null)
                                        {
                                            var pMAnyComp = mp.previousMeetingsAnyComp;

                                            var mps = new TableHeadToHeadSummary();
                                            mps.matchId = mi.id;
                                            mps.homeWin = pMAnyComp.homeContestantWins;
                                            mps.awayWin = pMAnyComp.awayContestantWins;
                                            mps.draw = pMAnyComp.draws;
                                            mps.homeScore = pMAnyComp.homeContestantGoals;
                                            mps.awayScore = pMAnyComp.awayContestantGoals;
                                            var Id = dm.InsertOrUpdateHeadToHeadSummary(mps);
                                        }

                                        if (mp.previousMeetingsAnyComp != null)
                                        {
                                            if (mp.previousMeetingsAnyComp.match != null)
                                            {
                                                foreach (var pm in mp.previousMeetingsAnyComp.match.Take(6))
                                                {
                                                    try
                                                    {
                                                        var hthm = new TableHeadToHeadMatches();
                                                        hthm.matchId = mi.id;
                                                        hthm.hthMatchId = pm.id;
                                                        hthm.competitionId = pm.competitionId;
                                                        hthm.tcalendarId = pm.tournamentCalendarId;
                                                        hthm.teamAId = pm.contestants.homeContestantId;
                                                        hthm.teamAName = pm.contestants.homeContestantName;
                                                        hthm.teamBId = pm.contestants.awayContestantId;
                                                        hthm.teamBName = pm.contestants.awayContestantName;
                                                        hthm.homeScore = int.TryParse(pm.contestants.homeScore, out tInt) ? tInt : 0;
                                                        hthm.awayScore = int.TryParse(pm.contestants.awayScore, out tInt) ? tInt : 0;
                                                        hthm.matchDate = DateTime.TryParse(pm.date, out tDate) ? tDate : DateTime.MinValue;
                                                        var Id = dm.InsertOrUpdateHeadToHeadMatches(hthm);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        logError(ex.StackTrace);
                                                    }
                                                }
                                            }
                                        }
                                     

                                        if (mp.formAnyComp != null)
                                        {
                                            foreach (var fl in mp.formAnyComp)
                                            {
                                                foreach (var pm in fl.match)
                                                {
                                                    try
                                                    {
                                                        var hthm = new TableMatchForm();
                                                        hthm.matchId = mi.id;
                                                        hthm.contestantId = fl.contestantId;
                                                        hthm.hthMatchId = pm.id;
                                                        hthm.competitionId = pm.competitionId;
                                                        hthm.tcalendarId = pm.tournamentCalendarId;
                                                        hthm.teamAId = pm.contestants.homeContestantId;
                                                        hthm.teamAName = pm.contestants.homeContestantName;
                                                        hthm.teamBId = pm.contestants.awayContestantId;
                                                        hthm.teamBName = pm.contestants.awayContestantName;
                                                        hthm.homeScore = int.TryParse(pm.contestants.homeScore, out tInt) ? tInt : 0;
                                                        hthm.awayScore = int.TryParse(pm.contestants.awayScore, out tInt) ? tInt : 0;
                                                        hthm.matchDate = DateTime.TryParse(pm.date, out tDate) ? tDate : DateTime.MinValue;
                                                        var Id = dm.InsertOrUpdateMatchForm(hthm);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        logError(ex.StackTrace);
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    logInfo("Match preview already updated for played match");
                                }

                            }
                            catch (Exception ex)
                            {
                                logError(ex.StackTrace);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logError(ex.StackTrace);
            }
        }

        public JsonMatchData ReadcompListFromUrl(string url)
        {
            // Create a list of articles to loop through later.
            var matchList = new JsonMatchData();

            try
            {
                // Store the response from the Url into a string for parsing.
                string htmlResponse = _performHelper.GetHtmlResponseFromUrl(url);
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                matchList = js.Deserialize<JsonMatchData>(htmlResponse);       
            }
            catch (Exception ex)
            {
                
                logError(ex.Message);
                return matchList;
            }
        
            return matchList;
        }

        public JsonMatchData ReadcompListFromUrl(string url,PagerDuty.PagerDutyCaller pagerDutyCaller)
        {
            // Create a list of articles to loop through later.
            var matchList = new JsonMatchData();

            try
            {
                // Store the response from the Url into a string for parsing.
                string htmlResponse =_performHelper.GetHtmlResponseFromUrl(url);
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                matchList = js.Deserialize<JsonMatchData>(htmlResponse);
            }
            catch (Exception ex)
            {
                pagerDutyCaller.CallPagerDuty(ex.Message);
                logError(ex.Message);
                return matchList;
            }

            return matchList;
        }


        public JsonMatchPreviewData ReadcompListFromUrlMatchPreview(string url)
        {
            // Create a list of articles to loop through later.
            var matchList = new JsonMatchPreviewData();

            try
            {
                // Store the response from the Url into a string for parsing.
                string htmlResponse = _performHelper.GetHtmlResponseFromUrl(url);
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                matchList = js.Deserialize<JsonMatchPreviewData>(htmlResponse);
            }
            catch (Exception ex)
            {
                logError(ex.Message);
                return matchList;
            }
            return matchList;
        }

        #region Helper
        //public string GetHtmlResponseFromUrl(string url)
        //{
        //    logInfo("Reading from url : " + url);
        //    StringBuilder sb = new StringBuilder();
        //    byte[] buffer = new byte[8192];

        //    try
        //    {
        //        Uri uri = new Uri(url);

        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
        //        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //        Stream respStream = response.GetResponseStream();

        //        string tempString = "";
        //        int count = 0;

        //        do
        //        {
        //            count = respStream.Read(buffer, 0, buffer.Length);

        //            if (count != 0)
        //            {
        //                tempString = Encoding.UTF8.GetString(buffer, 0, count);
        //                sb.Append(tempString);
        //            }

        //        } while (count > 0);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return sb.ToString();
        //}
        #endregion
    }
}
