﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016
{
    public class JsonPersonCareer
    {
        public List<Person> person { get; set; }
        public string lastUpdated { get; set; }

        public class Stat
        {
            public string competitionId { get; set; }
            public string competitionName { get; set; }
            public string tournamentCalendarId { get; set; }
            public string tournamentCalendarName { get; set; }
            public int goals { get; set; }
            public int assists { get; set; }
            public int penaltyGoals { get; set; }
            public int appearances { get; set; }
            public int yellowCards { get; set; }
            public int secondYellowCards { get; set; }
            public int redCards { get; set; }
            public int substituteIn { get; set; }
            public int substituteOut { get; set; }
            public int subsOnBench { get; set; }
            public int minutesPlayed { get; set; }
            public int shirtNumber { get; set; }
            public string competitionFormat { get; set; }
        }

        public class Membership
        {
            public List<Stat> stat { get; set; }
            public string contestantId { get; set; }
            public string contestantType { get; set; }
            public string contestantName { get; set; }
            public string active { get; set; }
            public string startDate { get; set; }
            public string role { get; set; }
            public string endDate { get; set; }
        }

        public class Person
        {
            public List<Membership> membership { get; set; }
            public string id { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string matchName { get; set; }
            public string type { get; set; }
            public string position { get; set; }
            public string lastUpdated { get; set; }
            public string nationality { get; set; }
            public string nationalityId { get; set; }
            public string dateOfBirth { get; set; }
            public string placeOfBirth { get; set; }
            public string countryOfBirthId { get; set; }
            public string countryOfBirth { get; set; }
            public string height { get; set; }
            public string weight { get; set; }
            public string foot { get; set; }
            public string gender { get; set; }
            public string status { get; set; }
        }
    }
}
