﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using log4net.Config;
using PetaPoco;

namespace PerformSDAPIDataIngest2016
{
    public class IngestionBase
    {

        #region Variables
        public ILog log;
        public string connectionType = ConfigurationManager.AppSettings["ConnectionType"];
        //public string connectionString;
        public bool outputToConsole = Convert.ToBoolean(ConfigurationManager.AppSettings["outputToConsole"]);
        public Database kendb;
        #endregion

        #region Constructor
        public IngestionBase()
        {
            //connectionString = ConfigurationManager.ConnectionStrings[connectionType].ConnectionString;
            kendb = new Database(connectionType);
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(typeof(IngestionBase));
        }
        #endregion

        #region Log Functions
        public void logDebug(string msg)
        {
            if (outputToConsole)
            {
                Console.WriteLine(msg);
            }

            log.Debug(msg);
        }

        public void logInfo(string msg)
        {
            if (outputToConsole)
            {
                Console.WriteLine(msg);
            }
            log.Info(msg);
        }

        public void logError(string msg)
        {
            if (outputToConsole)
            {
                Console.WriteLine(msg);
            }
            log.Error(msg);
        }
        #endregion
    }
}
