﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace PerformSDAPIDataIngest2016
{
    public class IngestPersonCareer : IngestionBase
    {
        private string performUrl = System.Configuration.ConfigurationManager.AppSettings["performUrlPersonCareer"];
        private string performTcId = System.Configuration.ConfigurationManager.AppSettings["performTcId"];
        private string performUuId = System.Configuration.ConfigurationManager.AppSettings["performUrlUuId"];
        Helper.StatsPerformHelper _performHelper;

        public IngestPersonCareer()
        {
            _performHelper = new Helper.StatsPerformHelper();
        }
        public void DoIngest(string playerId)
        {
            logInfo("Starting ingestion...");

            string dataUrl = performUrl + "/{0}?prsn={1}&_fmt=json&_rt=b";
            dataUrl = String.Format(dataUrl, performUuId, playerId);
            IngestUrl(dataUrl);

            logInfo("Completed ingestion.");
        }

        public void IngestUrl(string url)
        {
            var dm = new DatabaseManager(); 
            var tDate = DateTime.MinValue;

            try
            {
                var personList = ReadcompListFromUrl(url);
                if (personList != null)
                {
                    var lastUpdated = DateTime.TryParse(personList.lastUpdated, out tDate) ? tDate : DateTime.MinValue;
                    if (lastUpdated != DateTime.MinValue)
                    {
                        var selectedPerson = dm.getPerson(personList.person.FirstOrDefault().id);
                        if (selectedPerson != null)
                        {
                            if (lastUpdated <= selectedPerson.lastUpdated)
                            {
                                logInfo("Person Career already ingested.");
                                return;
                            }
                        }
                    }


                    var membershipList = personList.person.FirstOrDefault().membership;
                    if (membershipList != null)
                    {
                        foreach (var m in membershipList)
                        {
                            try
                            {
                                var ti = new TableContestant();
                                ti.id = m.contestantId;
                                ti.teamType = m.contestantType;
                                ti.name = m.contestantName;
                                ti.shortName = string.Empty;
                                ti.clubName = string.Empty;
                                ti.code = string.Empty;
                                var teamId = dm.InsertOrUpdateContestant(ti);

                                var pci = new TablePersonCareer();
                                pci.contestantId = m.contestantId;
                                pci.contestantType = m.contestantType;
                                pci.active = m.active;
                                pci.startDate = DateTime.TryParse(m.startDate, out tDate) ? tDate : DateTime.MinValue;
                                pci.endDate = DateTime.TryParse(m.endDate, out tDate) ? tDate : DateTime.MinValue;
                                pci.role = m.role;
                                pci.playerId = personList.person.FirstOrDefault().id;
                                var tpcId = dm.InsertOrUpdatePersonCareer(pci);

                                if (m.stat != null)
                                {
                                    foreach (var s in m.stat)
                                    {
                                        try
                                        {
                                            var pcs = new TablePersonCareerStats();
                                            pcs.goals = s.goals;
                                            pcs.assists = s.assists;
                                            pcs.penaltyGoals = s.penaltyGoals;
                                            pcs.appearances = s.appearances;
                                            pcs.yellowCards = s.yellowCards;
                                            pcs.secondYellowCards = s.secondYellowCards;
                                            pcs.redCards = s.redCards;
                                            pcs.substituteIn = s.substituteIn;
                                            pcs.substituteOut = s.substituteOut;
                                            pcs.subsOnBench = s.subsOnBench;
                                            pcs.minutesPlayed = s.minutesPlayed;
                                            pcs.shirtNumber = s.shirtNumber;
                                            pcs.competitionId = s.competitionId;
                                            pcs.tournamentCalendarId = s.tournamentCalendarId;
                                            pcs.contestantId = m.contestantId;
                                            pcs.playerId = personList.person.FirstOrDefault().id;
                                            var tpcsId = dm.InsertOrUpdatePersonCareerStats(pcs);
                                        }
                                        catch (Exception ex)
                                        {
                                            logError(ex.StackTrace);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                logError(ex.StackTrace);
                            }
                        }

                        var pi = new TablePerson();
                        pi.id = personList.person.FirstOrDefault().id;
                        pi.lastUpdated = lastUpdated;
                        var playerId = dm.InsertOrUpdatePerson(pi);
                    }
                }
            }
            catch (Exception ex)
            {
                logError(ex.StackTrace);
            }
        }

        public JsonPersonCareer ReadcompListFromUrl(string url)
        {
            // Create a list of articles to loop through later.
            var squadList = new JsonPersonCareer();

            try
            {
                // Store the response from the Url into a string for parsing.
                string htmlResponse = _performHelper.GetHtmlResponseFromUrl(url);
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                squadList = js.Deserialize<JsonPersonCareer>(htmlResponse);       
            }
            catch (Exception ex)
            {
                logError(ex.Message);
                return squadList;
            }
            return squadList;
        }

        #region Helper
        //public string GetHtmlResponseFromUrl(string url)
        //{
        //    logInfo("Reading from url : " + url);
        //    StringBuilder sb = new StringBuilder();
        //    byte[] buffer = new byte[8192];

        //    try
        //    {
        //        Uri uri = new Uri(url);

        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
        //        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //        Stream respStream = response.GetResponseStream();

        //        string tempString = "";
        //        int count = 0;

        //        do
        //        {
        //            count = respStream.Read(buffer, 0, buffer.Length);

        //            if (count != 0)
        //            {
        //                tempString = Encoding.UTF8.GetString(buffer, 0, count);
        //                sb.Append(tempString);
        //            }

        //        } while (count > 0);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return sb.ToString();
        //}
        #endregion
    }
}
