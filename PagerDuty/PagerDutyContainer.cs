﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016.PagerDuty
{
    public class PagerDutyContainer
    {
        public PagerDutyPayload payload { get; set; }
        public string routing_key { get; set; }
        public string event_action { get; set; }

        public PagerDutyContainer(string key, string summary, string description)
        {
            payload = new PagerDutyPayload();
            payload.summary = summary;
            payload.timestamp = DateTime.Now;
            payload.severity = "critical";
            payload.source = "Perform Soccer Result Ingestion";
            this.routing_key = key;
            this.event_action = "trigger";

            payload.custom_details = new PagerDutyCustomDetails();
            payload.custom_details.description = description;
        }
    }
}
