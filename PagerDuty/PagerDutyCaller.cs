﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace PerformSDAPIDataIngest2016.PagerDuty
{
    public class PagerDutyCaller
    {
        public ILog log;
        public PagerDutyCaller()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(typeof(PagerDutyCaller));
        }

        public void CallPagerDuty(string Msg, bool isSend = true)
        {
            Console.WriteLine("Call Pager Duty");
            Console.WriteLine("INPUT MESSAGE=>" + Msg);
            if (isSend)
            {
                if (PerformIngestionConfig.PagerDutyEnable)
                {
                    log.Error(Msg);
                    try
                    {
                      
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(PerformIngestionConfig.PagerDutyEndpoint);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        //Create Pager Duty payload
                        PagerDutyContainer pagerPayload = new PagerDutyContainer(PerformIngestionConfig.PagerDutyKey, "Perform Soccer Result Ingestion", Msg);

                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            streamWriter.Write(JsonConvert.SerializeObject(pagerPayload));
                        }
                        Console.WriteLine("Call Pager Duty");
                        // Receive Pager Duty response
                        HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        // Display Pager Duty response
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            string result = streamReader.ReadToEnd();
                            if (!String.IsNullOrEmpty(result))
                            {
                                PagerDutyResponse resp = JsonConvert.DeserializeObject<PagerDutyResponse>(result);
                                if (resp != null && (int)httpResponse.StatusCode == 202)
                                {
                                    log.Info(string.Format("PagerDuty TRIGGERED:{0}", Msg));
                                }
                            }
                        }
                        Console.WriteLine("Finish sending to Pagerduty");
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine("EXCEPTION DURING CALL PAGER DUTY=>"+ex.Message);
                    }
                  
                }
            }
        }
    }
}
