﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016.PagerDuty
{
    public class PagerDutyPayload
    {
        public string summary { get; set; }

        public DateTime timestamp { get; set; }

        public string severity { get; set; }

        public string source { get; set; }

        public PagerDutyCustomDetails custom_details { get; set; }

    }
}
