﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016.PagerDuty
{
    public class PagerDutyResponse
    {
        public string status { get; set; }
        public string message { get; set; }

        public string dedup_key { get; set; }
    }
}
