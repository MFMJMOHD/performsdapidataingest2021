﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformSDAPIDataIngest2016
{
    public class JsonMatchStatData
    {
        public MatchInfo matchInfo { get; set; }
        public LiveData liveData { get; set; }

        public class Sport
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Ruleset
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Country
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Competition
        {
            public string id { get; set; }
            public string name { get; set; }
            public Country country { get; set; }
        }

        public class TournamentCalendar
        {
            public string id { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string name { get; set; }
        }

        public class Stage
        {
            public string id { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string name { get; set; }
        }

        public class Contestant
        {
            public string id { get; set; }
            public string name { get; set; }
            public Country country { get; set; }
            public string position { get; set; }
        }

        public class Venue
        {
            public string id { get; set; }
            public string shortName { get; set; }
            public string longName { get; set; }
        }

        public class MatchInfo
        {
            public string description { get; set; }
            public Sport sport { get; set; }
            public Ruleset ruleset { get; set; }
            public Competition competition { get; set; }
            public TournamentCalendar tournamentCalendar { get; set; }
            public Stage stage { get; set; }
            public List<Contestant> contestant { get; set; }
            public Venue venue { get; set; }
            public string id { get; set; }
            public string date { get; set; }
            public string time { get; set; }
            public string lastUpdated { get; set; }
        }

        public class Period
        {
            public int id { get; set; }
            public string start { get; set; }
            public string end { get; set; }
            public int lengthMin { get; set; }
            public int lengthSec { get; set; }
        }

        public class Ht
        {
            public int home { get; set; }
            public int away { get; set; }
        }

        public class Ft
        {
            public int home { get; set; }
            public int away { get; set; }
        }

        public class Et
        {
            public int home { get; set; }
            public int away { get; set; }
        }

        public class Pen
        {
            public int home { get; set; }
            public int away { get; set; }
        }

        public class Total
        {
            public int home { get; set; }
            public int away { get; set; }
        }

        public class Aggregate
        {
            public int home { get; set; }
            public int away { get; set; }
        }
         
        public class Scores
        {
            public Ht ht { get; set; }
            public Ft ft { get; set; }
            public Et et { get; set; }
            public Pen pen { get; set; }
            public Total total { get; set; }
            public Aggregate aggregate { get; set; }
        }

        public class MatchDetails
        {
            public int periodId { get; set; }
            public string matchStatus { get; set; }
            public List<Period> period { get; set; }
            public Scores scores { get; set; }
            public string winner { get; set; }
            public int matchLengthMin { get; set; }
            public int matchLengthSec { get; set; }
            public int matchTime { get; set; }
            public string aggregateWinnerId { get; set; }
        }

        public class Goal
        {
            public string contestantId { get; set; }
            public int periodId { get; set; }
            public int timeMin { get; set; }
            public string lastUpdated { get; set; }
            public string type { get; set; }
            public string scorerId { get; set; }
            public string scorerName { get; set; }
            public string assistPlayerId { get; set; }
            public string assistPlayerName { get; set; }
            public Int64 optaEventId { get; set; }
        }

        public class MissedPen
        {
            public string contestantId { get; set; }
            public int periodId { get; set; }
            public int timeMin { get; set; }
            public string lastUpdated { get; set; }
            public string type { get; set; }
            public string playerId { get; set; }
            public string playerName { get; set; }
            public Int64 optaEventId { get; set; }
        }

        public class Card
        {
            public string contestantId { get; set; }
            public int periodId { get; set; }
            public int timeMin { get; set; }
            public string lastUpdated { get; set; }
            public string type { get; set; }
            public string playerId { get; set; }
            public string playerName { get; set; }
            public Int64 optaEventId { get; set; }
        }

        public class Substitute
        {
            public string contestantId { get; set; }
            public int periodId { get; set; }
            public int timeMin { get; set; }
            public string lastUpdated { get; set; }
            public string playerOnId { get; set; }
            public string playerOnName { get; set; }
            public string playerOffId { get; set; }
            public string playerOffName { get; set; }
        }

        public class PenaltyShot
        {
            public string contestantId { get; set; }
            public string lastUpdated { get; set; }
            public string outcome { get; set; }
            public string playerId { get; set; }
            public string scorerName { get; set; }
            public int teamPenaltyNumber { get; set; }
            public Int64 optaEventId { get; set; }
        }

        public class Player
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string matchName { get; set; }
            public string playerId { get; set; }
            public int shirtNumber { get; set; }
            public string position { get; set; }
            public string positionSide { get; set; }
            public List<Stat> stat { get; set; }
        }

        public class TeamOfficial
        {
            public string id { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string type { get; set; }
        }

        public class Stat
        {
            public string fh { get; set; }
            public string sh { get; set; }
            public string efh { get; set; }
            public string esh { get; set; }
            public string type { get; set; }
            public string value { get; set; }
        }

        public class LineUp
        {
            public List<Player> player { get; set; }
            public TeamOfficial teamOfficial { get; set; }
            public List<Stat> stat { get; set; }
            public string contestantId { get; set; }
        }

        public class MatchOfficial
        {
            public string id { get; set; }
            public string type { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
        }

        public class MatchDetailsExtra
        {
            public string attendance { get; set; }
            public string weather { get; set; }
            public List<MatchOfficial> matchOfficial { get; set; }
        }

        public class LiveData
        {
            public MatchDetails matchDetails { get; set; }
            public List<Goal> goal { get; set; }
            public List<MissedPen> missedPen { get; set; }
            public List<Card> card { get; set; }
            public List<Substitute> substitute { get; set; }
            public List<PenaltyShot> penaltyShot { get; set; }
            public List<LineUp> lineUp { get; set; }
            public MatchDetailsExtra matchDetailsExtra { get; set; }
        }
    }
}
